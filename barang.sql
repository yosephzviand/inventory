-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2021 at 08:36 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `masadibarang`
--

-- --------------------------------------------------------

--
-- Table structure for table `angsuran`
--

CREATE TABLE `angsuran` (
  `idpembayaran` int(11) NOT NULL,
  `idtransaksi` int(255) DEFAULT NULL,
  `angsuran` int(11) DEFAULT NULL,
  `tglangsuran` date DEFAULT NULL,
  `user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `angsuran`
--

INSERT INTO `angsuran` (`idpembayaran`, `idtransaksi`, `angsuran`, `tglangsuran`, `user`) VALUES
(6, 2102130002, 500000, '2021-02-13', NULL),
(7, 2102130003, 200000, '2021-02-13', NULL),
(8, 2102130003, 50000, '2021-02-13', NULL),
(9, 2102130003, 50000, '2021-02-13', NULL),
(10, 2102130003, 50000, '2021-02-13', NULL),
(11, 2102130002, 500000, '2021-02-13', 10),
(12, 2102130004, 300000, '2021-02-13', 10),
(13, 2102130004, 200000, '2021-02-13', 10),
(14, 2102130004, 50000, '2021-02-13', 10),
(15, 2102130004, 150000, '2021-02-13', 10);

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `idbarang` int(255) NOT NULL,
  `kodebarang` varchar(255) DEFAULT NULL,
  `namabarang` varchar(255) DEFAULT NULL,
  `serialnumber` varchar(255) DEFAULT NULL,
  `merek` varchar(255) DEFAULT NULL,
  `stokawal` int(255) DEFAULT NULL,
  `stok` int(255) DEFAULT NULL,
  `tglmasuk` date DEFAULT NULL,
  `idsupp` int(255) DEFAULT NULL,
  `idkategori` int(255) DEFAULT NULL,
  `hargabeli` double(255,0) DEFAULT NULL,
  `garansi` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`idbarang`, `kodebarang`, `namabarang`, `serialnumber`, `merek`, `stokawal`, `stok`, `tglmasuk`, `idsupp`, `idkategori`, `hargabeli`, `garansi`) VALUES
(2, 'LHG0001', 'ROCKET', 'OOE0303', 'UBIQUITY', 18, 16, '2021-01-19', 1, 1, 500000, '2021-02-28'),
(16, 'AP0001', 'AP TENDA O3', 'OUE9909P', 'TENDA', 12, 11, '2021-01-20', 1, 4, 450000, '2021-03-31'),
(17, 'SW00001', 'CCR 1000', 'OOI039394', 'MIKROTIK', 22, 22, '2021-01-20', 1, 3, 1200000, '2021-02-25'),
(18, 'MK0093', 'RB950G', 'OUH999DJS', 'MIKROTIK', 13, 12, '2021-01-21', 1, 4, 950000, '2021-05-31'),
(19, 'RB100', 'RB 1100', 'OOI898U', 'MIKROTIK', 10, 10, '2021-01-23', 1, 3, 1200000, '2021-03-01');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `idkategori` int(255) NOT NULL,
  `namakategori` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`idkategori`, `namakategori`) VALUES
(1, 'LHG'),
(3, 'SWITCH'),
(4, 'AP');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `idlevel` int(255) NOT NULL,
  `namalevel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`idlevel`, `namalevel`) VALUES
(1, 'Admin'),
(2, 'Karyawan');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `idsup` int(255) NOT NULL,
  `namasup` varchar(255) DEFAULT NULL,
  `alamatsup` varchar(255) DEFAULT NULL,
  `notelpsup` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`idsup`, `namasup`, `alamatsup`, `notelpsup`) VALUES
(1, 'YOSEPHUS W', 'SUDIMORO KELOR KARANGMOJO', '08232000');

-- --------------------------------------------------------

--
-- Table structure for table `trans`
--

CREATE TABLE `trans` (
  `idtransaksi` varchar(255) DEFAULT NULL,
  `idtrans` int(255) NOT NULL,
  `namapembeli` varchar(255) DEFAULT NULL,
  `tglkeluar` date DEFAULT NULL,
  `iduser` varchar(255) DEFAULT NULL,
  `nomorsurat` varchar(255) DEFAULT NULL,
  `total` int(255) DEFAULT NULL,
  `bayar` int(255) DEFAULT NULL,
  `sisa` int(255) DEFAULT NULL,
  `tglbayar` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trans`
--

INSERT INTO `trans` (`idtransaksi`, `idtrans`, `namapembeli`, `tglkeluar`, `iduser`, `nomorsurat`, `total`, `bayar`, `sisa`, `tglbayar`) VALUES
('2102130002', 237, 'PANI', '2021-02-13', '3', '0002/GESIT/II/2021', 1000000, 1000000, 0, '2021-02-13'),
('2102130003', 238, 'WENI', '2021-02-13', '3', '0003/GESIT/II/2021', 350000, 350000, 0, '2021-02-13'),
('2102130004', 239, 'NINO', '2021-02-02', '10', '0004/GESIT/II/2021', 850000, 700000, 150000, '2021-02-13');

--
-- Triggers `trans`
--
DELIMITER $$
CREATE TRIGGER `nomorid` BEFORE INSERT ON `trans` FOR EACH ROW BEGIN
	declare billing char (12);
	DECLARE Y CHAR(2);
	DECLARE M CHAR(2);
	DECLARE Z CHAR(2);
	DECLARE URUT INT;
	DECLARE A INT;
	
	SET M = (SELECT DATE_FORMAT(CURDATE(),'%m'));
	SET Y = (SELECT DATE_FORMAT(CURDATE(),'%yy'));
	SET Z = (SELECT DATE_FORMAT(CURDATE(),'%d'));
	SET A = (SELECT max(idtransaksi) from trans WHERE MONTH(tglkeluar)=M);
	
	if A is null then
		SET URUT = (SELECT count(idtransaksi) from trans WHERE MONTH(tglkeluar)=M);
	else
		SET URUT = (SELECT max(SUBSTR(idtransaksi,7,4)) from trans WHERE MONTH(tglkeluar)=M);
	end if ;
	
	set billing = CONCAT(Y,M,Z,LPAD(URUT+1,4,0));
set new.idtransaksi=billing;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `nomorsurat` BEFORE INSERT ON `trans` FOR EACH ROW BEGIN
	
	DECLARE NS CHAR(20);
	DECLARE M CHAR(2);
	DECLARE Y CHAR(4);
	DECLARE ROMAWI CHAR(2);
	DECLARE A INT;
	DECLARE URUT INT;
	
	
	SET M = (SELECT DATE_FORMAT(CURDATE(),'%m'));
	SET Y = (SELECT DATE_FORMAT(CURDATE(),'%Y'));
	SET A = (SELECT max(nomorsurat) from trans WHERE MONTH(tglkeluar)=M);
	
	if A is null then
		SET URUT = (SELECT count(nomorsurat) from trans WHERE MONTH(tglkeluar)=M);
	else
		SET URUT = (SELECT max(SUBSTR(nomorsurat,1,4)) from trans WHERE MONTH(tglkeluar)=M);
	end if ;
	
	SET ROMAWI = CASE 
	WHEN M = 01 THEN
		'I'
		WHEN M = 02 THEN
		'II'
		WHEN M = 03 THEN
		'III'
		WHEN M = 04 THEN
		'IV'
		WHEN M = 05 THEN
		'V'
		WHEN M = 06 THEN
		'VI'
		WHEN M = 07 THEN
		'VII'
		WHEN M = 08 THEN
		'VIII'
		WHEN M = 09 THEN
		'IX'
		WHEN M = 10 THEN
		'X'
		WHEN M = 11 THEN
		'XI'
	ELSE
		'XII'
END;
	set NS = CONCAT(LPAD(urut+1,4,0),'/GESIT/',ROMAWI,'/',Y);
	
	SET NEW.nomorsurat=NS;
	END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `nomorupdate` AFTER INSERT ON `trans` FOR EACH ROW BEGIN
 UPDATE transaksi SET idtransaksi = new.idtransaksi where idtransaksi = new.idtransaksi;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `idkeluar` int(255) NOT NULL,
  `idtransaksi` varchar(255) DEFAULT NULL,
  `idkategori` varchar(255) DEFAULT NULL,
  `idbarang` varchar(255) DEFAULT NULL,
  `hargajual` double(255,0) DEFAULT NULL,
  `jmlkeluar` int(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `tglhapus` date DEFAULT NULL,
  `userhapus` int(255) DEFAULT NULL,
  `tglkeluar` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`idkeluar`, `idtransaksi`, `idkategori`, `idbarang`, `hargajual`, `jmlkeluar`, `keterangan`, `tglhapus`, `userhapus`, `tglkeluar`) VALUES
(418, '2102130002', '1', '2', 500000, 2, 'DIJUAL', NULL, NULL, NULL),
(419, '2102130003', '4', '16', 350000, 1, 'DIJUAL', NULL, NULL, NULL),
(420, '2102130004', '4', '18', 850000, 1, 'dijual', NULL, NULL, NULL);

--
-- Triggers `transaksi`
--
DELIMITER $$
CREATE TRIGGER `BARANG_KELUAR` AFTER INSERT ON `transaksi` FOR EACH ROW BEGIN
 UPDATE barang SET stok=stok-NEW.jmlkeluar
 WHERE idbarang=NEW.idbarang;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_barang` AFTER DELETE ON `transaksi` FOR EACH ROW BEGIN
 UPDATE barang SET stok= stok + old.jmlkeluar
 WHERE idbarang = old.idbarang ;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idusers` int(100) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idusers`, `username`, `password`, `level`, `nama`) VALUES
(3, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 1, 'Ngadimin'),
(10, 'yosephus', '827ccb0eea8a706c4c34a16891f84e7b', 2, 'yosephus');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_barang`
-- (See below for the actual view)
--
CREATE TABLE `vw_barang` (
`idbarang` int(255)
,`kodebarang` varchar(255)
,`namabarang` varchar(255)
,`serialnumber` varchar(255)
,`merek` varchar(255)
,`stokawal` int(255)
,`stok` int(255)
,`tglmasuk` date
,`idsupp` int(255)
,`idkategori` int(255)
,`hargabeli` double(255,0)
,`totalharga` double(17,0)
,`garansi` date
,`namasup` varchar(255)
,`namakategori` varchar(255)
,`alamatsup` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_charts`
-- (See below for the actual view)
--
CREATE TABLE `vw_charts` (
`idbarang` int(255)
,`namabarang` varchar(255)
,`stok` int(255)
,`jumlah` decimal(65,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_charts_kategori`
-- (See below for the actual view)
--
CREATE TABLE `vw_charts_kategori` (
`idkategori` int(255)
,`jumlah` decimal(65,0)
,`namakategori` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_keluar`
-- (See below for the actual view)
--
CREATE TABLE `vw_keluar` (
`namapembeli` varchar(255)
,`tglkeluar` date
,`iduser` varchar(255)
,`idkategori` varchar(255)
,`idbarang` varchar(255)
,`idtransaksi` varchar(255)
,`hargajual` double(255,0)
,`jmlkeluar` int(255)
,`namakategori` varchar(255)
,`namabarang` varchar(255)
,`kodebarang` varchar(255)
,`serialnumber` varchar(255)
,`merek` varchar(255)
,`stokawal` int(255)
,`stok` int(255)
,`tglmasuk` date
,`idsupp` int(255)
,`hargabeli` double(255,0)
,`garansi` date
,`nomorsurat` varchar(255)
,`keterangan` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_stok_keluar`
-- (See below for the actual view)
--
CREATE TABLE `vw_stok_keluar` (
`idtransaksi` varchar(255)
,`totalharga` double(17,0)
,`stokkeluar` decimal(65,0)
,`tglkeluar` date
,`namapembeli` varchar(255)
,`total` int(255)
,`bayar` int(255)
,`sisa` int(255)
,`tglbayar` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_users`
-- (See below for the actual view)
--
CREATE TABLE `vw_users` (
`idusers` int(100)
,`username` varchar(255)
,`password` varchar(255)
,`level` int(11)
,`nama` varchar(255)
,`namalevel` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `vw_barang`
--
DROP TABLE IF EXISTS `vw_barang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_barang`  AS SELECT `barang`.`idbarang` AS `idbarang`, `barang`.`kodebarang` AS `kodebarang`, `barang`.`namabarang` AS `namabarang`, `barang`.`serialnumber` AS `serialnumber`, `barang`.`merek` AS `merek`, `barang`.`stokawal` AS `stokawal`, `barang`.`stok` AS `stok`, `barang`.`tglmasuk` AS `tglmasuk`, `barang`.`idsupp` AS `idsupp`, `barang`.`idkategori` AS `idkategori`, `barang`.`hargabeli` AS `hargabeli`, `barang`.`stokawal`* `barang`.`hargabeli` AS `totalharga`, `barang`.`garansi` AS `garansi`, `supplier`.`namasup` AS `namasup`, `kategori`.`namakategori` AS `namakategori`, `supplier`.`alamatsup` AS `alamatsup` FROM ((`barang` join `kategori` on(`barang`.`idkategori` = `kategori`.`idkategori`)) join `supplier` on(`barang`.`idsupp` = `supplier`.`idsup`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_charts`
--
DROP TABLE IF EXISTS `vw_charts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_charts`  AS SELECT `barang`.`idbarang` AS `idbarang`, `barang`.`namabarang` AS `namabarang`, `barang`.`stok` AS `stok`, sum(`transaksi`.`jmlkeluar`) AS `jumlah` FROM (`barang` left join `transaksi` on(`barang`.`idbarang` = `transaksi`.`idbarang`)) GROUP BY `barang`.`idbarang` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_charts_kategori`
--
DROP TABLE IF EXISTS `vw_charts_kategori`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_charts_kategori`  AS SELECT `kategori`.`idkategori` AS `idkategori`, sum(`vw_barang`.`stok`) AS `jumlah`, `kategori`.`namakategori` AS `namakategori` FROM (`vw_barang` join `kategori` on(`vw_barang`.`idkategori` = `kategori`.`idkategori`)) GROUP BY `kategori`.`idkategori` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_keluar`
--
DROP TABLE IF EXISTS `vw_keluar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_keluar`  AS SELECT `trans`.`namapembeli` AS `namapembeli`, `trans`.`tglkeluar` AS `tglkeluar`, `trans`.`iduser` AS `iduser`, `transaksi`.`idkategori` AS `idkategori`, `transaksi`.`idbarang` AS `idbarang`, `transaksi`.`idtransaksi` AS `idtransaksi`, `transaksi`.`hargajual` AS `hargajual`, `transaksi`.`jmlkeluar` AS `jmlkeluar`, `kategori`.`namakategori` AS `namakategori`, `barang`.`namabarang` AS `namabarang`, `barang`.`kodebarang` AS `kodebarang`, `barang`.`serialnumber` AS `serialnumber`, `barang`.`merek` AS `merek`, `barang`.`stokawal` AS `stokawal`, `barang`.`stok` AS `stok`, `barang`.`tglmasuk` AS `tglmasuk`, `barang`.`idsupp` AS `idsupp`, `barang`.`hargabeli` AS `hargabeli`, `barang`.`garansi` AS `garansi`, `trans`.`nomorsurat` AS `nomorsurat`, `transaksi`.`keterangan` AS `keterangan` FROM (((`trans` join `transaksi` on(`trans`.`idtransaksi` = `transaksi`.`idtransaksi`)) join `kategori` on(`transaksi`.`idkategori` = `kategori`.`idkategori`)) join `barang` on(`transaksi`.`idbarang` = `barang`.`idbarang`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_stok_keluar`
--
DROP TABLE IF EXISTS `vw_stok_keluar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_stok_keluar`  AS SELECT `transaksi`.`idtransaksi` AS `idtransaksi`, sum(`transaksi`.`hargajual` * `transaksi`.`jmlkeluar`) AS `totalharga`, sum(`transaksi`.`jmlkeluar`) AS `stokkeluar`, `trans`.`tglkeluar` AS `tglkeluar`, `trans`.`namapembeli` AS `namapembeli`, `trans`.`total` AS `total`, `trans`.`bayar` AS `bayar`, `trans`.`sisa` AS `sisa`, `trans`.`tglbayar` AS `tglbayar` FROM (`transaksi` join `trans` on(`transaksi`.`idtransaksi` = `trans`.`idtransaksi`)) GROUP BY `transaksi`.`idtransaksi` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_users`
--
DROP TABLE IF EXISTS `vw_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_users`  AS SELECT `users`.`idusers` AS `idusers`, `users`.`username` AS `username`, `users`.`password` AS `password`, `users`.`level` AS `level`, `users`.`nama` AS `nama`, `level`.`namalevel` AS `namalevel` FROM (`level` join `users` on(`level`.`idlevel` = `users`.`level`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `angsuran`
--
ALTER TABLE `angsuran`
  ADD PRIMARY KEY (`idpembayaran`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`idbarang`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`idkategori`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`idlevel`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`idsup`);

--
-- Indexes for table `trans`
--
ALTER TABLE `trans`
  ADD PRIMARY KEY (`idtrans`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`idkeluar`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idusers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `angsuran`
--
ALTER TABLE `angsuran`
  MODIFY `idpembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `idbarang` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `idkategori` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `idlevel` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `trans`
--
ALTER TABLE `trans`
  MODIFY `idtrans` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `idkeluar` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=421;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idusers` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
