-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2021 at 10:31 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `masadibarang`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `idbarang` int(255) NOT NULL,
  `kodebarang` varchar(255) DEFAULT NULL,
  `namabarang` varchar(255) DEFAULT NULL,
  `serialnumber` varchar(255) DEFAULT NULL,
  `merek` varchar(255) DEFAULT NULL,
  `stokawal` int(255) DEFAULT NULL,
  `stok` int(255) DEFAULT NULL,
  `tglmasuk` date DEFAULT NULL,
  `idsupp` int(255) DEFAULT NULL,
  `idkategori` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`idbarang`, `kodebarang`, `namabarang`, `serialnumber`, `merek`, `stokawal`, `stok`, `tglmasuk`, `idsupp`, `idkategori`) VALUES
(2, 'LHG0001', 'ROCKET', 'OOE0303', 'UBIQUITY', 15, 9, '2021-01-19', 1, 1),
(16, 'AP0001', 'AP TENDA O3', 'OUE9909P', 'TENDA', 12, 10, '2021-01-20', 1, 4),
(17, 'SW00001', 'CCR 1000', 'OOI039394', 'MIKROTIK', 22, 22, '2021-01-20', 1, 3),
(18, 'MK0093', 'RB950G', 'OUH999DJS', 'MIKROTIK', 13, 11, '2021-01-21', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `idkategori` int(255) NOT NULL,
  `namakategori` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`idkategori`, `namakategori`) VALUES
(1, 'LHG'),
(3, 'SWITCH'),
(4, 'AP');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `idlevel` int(255) NOT NULL,
  `namalevel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`idlevel`, `namalevel`) VALUES
(1, 'Admin'),
(2, 'Karyawan');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `idsup` int(255) NOT NULL,
  `namasup` varchar(255) DEFAULT NULL,
  `alamatsup` varchar(255) DEFAULT NULL,
  `notelpsup` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`idsup`, `namasup`, `alamatsup`, `notelpsup`) VALUES
(1, 'YOSEPHUS W', 'SUDIMORO KELOR KARANGMOJO', '08232000');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `idtransaksi` int(255) NOT NULL,
  `idkategori` varchar(255) DEFAULT NULL,
  `idbarang` varchar(255) DEFAULT NULL,
  `tglkeluar` date DEFAULT NULL,
  `jmlkeluar` int(255) DEFAULT NULL,
  `namapengambil` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`idtransaksi`, `idkategori`, `idbarang`, `tglkeluar`, `jmlkeluar`, `namapengambil`) VALUES
(1, '1', '2', '2021-01-20', 1, 'mervia'),
(2, '1', '2', '2021-01-20', 5, 'yosua'),
(3, '4', '16', '2021-01-20', 2, 'etin'),
(4, '4', '18', '2021-01-20', 2, 'ADI');

--
-- Triggers `transaksi`
--
DELIMITER $$
CREATE TRIGGER `BARANG_KELUAR` AFTER INSERT ON `transaksi` FOR EACH ROW BEGIN
 UPDATE barang SET stok=stok-NEW.jmlkeluar
 WHERE idbarang=NEW.idbarang;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idusers` int(100) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idusers`, `username`, `password`, `level`, `nama`) VALUES
(3, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 1, 'Ngadimin');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_barang`
-- (See below for the actual view)
--
CREATE TABLE `vw_barang` (
`idbarang` int(255)
,`kodebarang` varchar(255)
,`namabarang` varchar(255)
,`serialnumber` varchar(255)
,`merek` varchar(255)
,`stokawal` int(255)
,`stok` int(255)
,`tglmasuk` date
,`idsupp` int(255)
,`idkategori` int(255)
,`namasup` varchar(255)
,`namakategori` varchar(255)
,`alamatsup` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_charts`
-- (See below for the actual view)
--
CREATE TABLE `vw_charts` (
`kodebarang` varchar(255)
,`idbarang` int(255)
,`namabarang` varchar(255)
,`serialnumber` varchar(255)
,`merek` varchar(255)
,`idkategori` varchar(255)
,`jumlah` decimal(65,0)
,`namakategori` varchar(255)
,`stok` int(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_charts_kategori`
-- (See below for the actual view)
--
CREATE TABLE `vw_charts_kategori` (
`jumlah` decimal(65,0)
,`kodebarang` varchar(255)
,`idbarang` int(255)
,`namabarang` varchar(255)
,`serialnumber` varchar(255)
,`merek` varchar(255)
,`idkategori` varchar(255)
,`namapengambil` varchar(255)
,`namakategori` varchar(255)
,`stok` int(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_keluar`
-- (See below for the actual view)
--
CREATE TABLE `vw_keluar` (
`kodebarang` varchar(255)
,`idbarang` int(255)
,`namabarang` varchar(255)
,`serialnumber` varchar(255)
,`merek` varchar(255)
,`idkategori` varchar(255)
,`idtransaksi` int(255)
,`tglkeluar` date
,`jmlkeluar` int(255)
,`namapengambil` varchar(255)
,`namakategori` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_users`
-- (See below for the actual view)
--
CREATE TABLE `vw_users` (
`idusers` int(100)
,`username` varchar(255)
,`password` varchar(255)
,`level` int(11)
,`nama` varchar(255)
,`namalevel` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `vw_barang`
--
DROP TABLE IF EXISTS `vw_barang`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_barang`  AS SELECT `barang`.`idbarang` AS `idbarang`, `barang`.`kodebarang` AS `kodebarang`, `barang`.`namabarang` AS `namabarang`, `barang`.`serialnumber` AS `serialnumber`, `barang`.`merek` AS `merek`, `barang`.`stokawal` AS `stokawal`, `barang`.`stok` AS `stok`, `barang`.`tglmasuk` AS `tglmasuk`, `barang`.`idsupp` AS `idsupp`, `barang`.`idkategori` AS `idkategori`, `supplier`.`namasup` AS `namasup`, `kategori`.`namakategori` AS `namakategori`, `supplier`.`alamatsup` AS `alamatsup` FROM ((`barang` join `kategori` on(`barang`.`idkategori` = `kategori`.`idkategori`)) join `supplier` on(`barang`.`idsupp` = `supplier`.`idsup`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_charts`
--
DROP TABLE IF EXISTS `vw_charts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_charts`  AS SELECT `vw_keluar`.`kodebarang` AS `kodebarang`, `vw_keluar`.`idbarang` AS `idbarang`, `vw_keluar`.`namabarang` AS `namabarang`, `vw_keluar`.`serialnumber` AS `serialnumber`, `vw_keluar`.`merek` AS `merek`, `vw_keluar`.`idkategori` AS `idkategori`, sum(`vw_keluar`.`jmlkeluar`) AS `jumlah`, `vw_keluar`.`namakategori` AS `namakategori`, `vw_barang`.`stok` AS `stok` FROM (`vw_keluar` join `vw_barang` on(`vw_keluar`.`idbarang` = `vw_barang`.`idbarang`)) GROUP BY `vw_keluar`.`idbarang` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_charts_kategori`
--
DROP TABLE IF EXISTS `vw_charts_kategori`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_charts_kategori`  AS SELECT sum(`vw_keluar`.`jmlkeluar`) AS `jumlah`, `vw_keluar`.`kodebarang` AS `kodebarang`, `vw_keluar`.`idbarang` AS `idbarang`, `vw_keluar`.`namabarang` AS `namabarang`, `vw_keluar`.`serialnumber` AS `serialnumber`, `vw_keluar`.`merek` AS `merek`, `vw_keluar`.`idkategori` AS `idkategori`, `vw_keluar`.`namapengambil` AS `namapengambil`, `vw_keluar`.`namakategori` AS `namakategori`, `vw_barang`.`stok` AS `stok` FROM (`vw_barang` join `vw_keluar` on(`vw_barang`.`idkategori` = `vw_keluar`.`idkategori`)) GROUP BY `vw_keluar`.`idkategori` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_keluar`
--
DROP TABLE IF EXISTS `vw_keluar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_keluar`  AS SELECT `barang`.`kodebarang` AS `kodebarang`, `barang`.`idbarang` AS `idbarang`, `barang`.`namabarang` AS `namabarang`, `barang`.`serialnumber` AS `serialnumber`, `barang`.`merek` AS `merek`, `transaksi`.`idkategori` AS `idkategori`, `transaksi`.`idtransaksi` AS `idtransaksi`, `transaksi`.`tglkeluar` AS `tglkeluar`, `transaksi`.`jmlkeluar` AS `jmlkeluar`, `transaksi`.`namapengambil` AS `namapengambil`, `kategori`.`namakategori` AS `namakategori` FROM ((`transaksi` join `kategori` on(`transaksi`.`idkategori` = `kategori`.`idkategori`)) join `barang` on(`barang`.`idbarang` = `transaksi`.`idbarang`)) ;

-- --------------------------------------------------------

--
-- Structure for view `vw_users`
--
DROP TABLE IF EXISTS `vw_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_users`  AS SELECT `users`.`idusers` AS `idusers`, `users`.`username` AS `username`, `users`.`password` AS `password`, `users`.`level` AS `level`, `users`.`nama` AS `nama`, `level`.`namalevel` AS `namalevel` FROM (`level` join `users` on(`level`.`idlevel` = `users`.`level`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`idbarang`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`idkategori`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`idlevel`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`idsup`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`idtransaksi`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idusers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `idbarang` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `idkategori` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `idlevel` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `idtransaksi` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idusers` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
