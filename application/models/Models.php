<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Models extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function insert($table,$data)
	{
		$res = $this->db->insert($table,$data);
		return $res;
	}

	public function update($table,$data,$where){
		$res = $this->db->update($table,$data, $where);
		return $res;
	}

	public function delete($table,$where){
		$res = $this->db->delete($table,$where);
		return $res;
	}

	public function countpelanggan()
	{
		$this->db->select('Count(idpelanggan) as jum');
		$hasil = $this->db->get('pelanggan');
		return $hasil->result();
	}

	public function grafik()
	{
		$hasil = $this->db->get('vw_charts');
		return $hasil->result();
	}

	public function grafikkategori()
	{
		$hasil = $this->db->get('vw_charts_kategori');
		return $hasil->result();
	}

	public function kategori()
	{
		$hasil = $this->db->get('kategori');
		return $hasil->result();
	}

	public function edit_kategori($id)
	{
		$this->db->where('idkategori', $id);
		$hasil = $this->db->get('kategori');
		return $hasil->result();
	}

	public function rak()
	{
		$hasil = $this->db->get('rak');
		return $hasil->result();
	}

	public function edit_rak($id)
	{
		$this->db->where('id', $id);
		$hasil = $this->db->get('rak');
		return $hasil->result();
	}

	public function supplier()
	{
		$hasil = $this->db->get('supplier');
		return $hasil->result();
	}

	public function edit_supplier($id)
	{
		$this->db->where('idsup', $id);
		$hasil = $this->db->get('supplier');
		return $hasil->result();
	}

	public function masuk()
	{
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function barang()
	{
		$this->db->where('stok !=', '0');
		$this->db->order_by('idkategori');
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function getbarang()
	{ 
		$this->db->where('stok <>', '0');
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function edit_masukid($id)
	{
		$this->db->where('idbarang', $id);
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function barangkategori($id)
	{
		$this->db->where('idkategori', $id);
		$this->db->order_by('idkategori');
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function baranglap($id)
	{
		$this->db->where('idkategori', $id);
		$hasil = $this->db->get('vw_lapbarang');
		return $hasil->result();
	}

	public function getbarangkategori($start, $end)
	{
		$this->db->where('idkategori', $start);
		$this->db->where('kodebarang', $end);
		$this->db->order_by('idkategori');
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function getsumbarangkategori($start, $end)
	{
		$this->db->select('sum(stok) as total');
		$this->db->where('idkategori', $start);
		$this->db->where('kodebarang', $end);
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function getsumbarangkat($start)
	{
		$this->db->select('sum(stok) as total');
		$this->db->where('idkategori', $start);
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function sumstokbrg()
	{
		$this->db->select('sum(stok) as total');
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function get_detailkeluar($id)
	{
		$this->db->where('idtransaksi', $id);
		$hasil = $this->db->get('vw_keluar');
		return $hasil->result();
	}

	public function get_detailangsuran($id)
	{
		$this->db->where('idtransaksi', $id);
		$hasil = $this->db->get('angsuran');
		return $hasil->result();
	}

	public function get_onekeluar($id)
	{
		$this->db->where('idtransaksi', $id);
		$hasil = $this->db->get('trans');
		return $hasil->result();
	}

	public function get_onebayar($id)
	{
		$this->db->where('idtransaksi', $id);
		$hasil = $this->db->get('trans');
		return $hasil->result_array();
	}

	public function sumget_onekeluar($id)
	{
		// $this->db->select('totalharga as total');
		$this->db->where('idtransaksi', $id);
		$hasil = $this->db->get('vw_stok_keluar');
		return $hasil->result();
	}

	public function maxid()
	{
		$this->db->select('max(idtransaksi) as idtransaksi');
		$hasil = $this->db->get('trans');
		return $hasil->result_array();
	}

	public function keluar()
	{
		$this->db->order_by('idtransaksi','desc');
		$hasil = $this->db->get('vw_stok_keluar');
		return $hasil->result();
	}

	public function edit_pengguna($id)
	{
		$this->db->where('idusers', $id);
		$hasil = $this->db->get('vw_users');
		return $hasil->result();
	}

	public function level()
	{
		$hasil = $this->db->get('level');
		return $hasil->result();
	}

	public function pengguna()
	{
		$hasil = $this->db->get('vw_users');
		return $hasil->result();
	}

	public function edit_tagihan($id)
	{
		$this->db->where('idtransaksi', $id);
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}

	public function cetak_masuk($start, $end)
	{
		$this->db->where('tglmasuk >=', $start);
		$this->db->where('tglmasuk <=', $end);
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function cetak_keluar($start, $end)
	{
		$this->db->where('tglkeluar >=', $start);
		$this->db->where('tglkeluar <=', $end);
		$this->db->order_by('idtransaksi');
		$this->db->order_by('tglkeluar');
		$hasil = $this->db->get('vw_keluar');
		return $hasil->result();
	}

	public function sum_cetak_keluar($start, $end)
	{
		$this->db->select('sum(jmlkeluar) as jumlah, sum(hargajual) as sumharga');
		$this->db->where('tglkeluar >=', $start);
		$this->db->where('tglkeluar <=', $end);
		$hasil = $this->db->get('vw_keluar');
		return $hasil->result();
	}

	public function sum_cetak_masuk($start, $end)
	{
		$this->db->select('sum(stokawal) as jumlah, sum(hargabeli) as sumharga, sum(totalharga) as sumtotal');
		$this->db->where('tglmasuk >=', $start);
		$this->db->where('tglmasuk <=', $end);
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function viewdatapelanggan($id)
	{
		$this->db->where('idpelanggan', $id);
		$hasil = $this->db->get('vw_tagihan');
		return $hasil->result();
	}

	public function getData($id)
	{
		$this->db->where('idbarang', $id);
		$hasil = $this->db->get('vw_barang');
		return $hasil->result();
	}

	public function multiSave($table, $data)
	{
		$jumlah = count($data);

		if ($jumlah > 0) {
			$res = $this->db->insert_batch($table, $data);
		}

		return $res;
	}

	public function getDinas()
	{
		
		$hasil = $this->db->get('dinas');
		return $hasil->result();
	}

	public function getDinasDetail($id)
	{
		$this->db->where('iddinas', $id);
		$hasil = $this->db->get('dinas_detail');
		return $hasil->result();
	}

	public function edit_dinas($id)
	{
		$this->db->where('id', $id);
		$hasil = $this->db->get('dinas');
		return $hasil->result();
	}

	public function getBarangTerjual()
	{
		$hasil = $this->db->get('vw_barang_terjual');
		return $hasil->result();
	}

	public function getUntungGlobal()
	{
		$hasil = $this->db->get('vw_untung_global');
		return $hasil->result();
	}

	public function getRekapUntung()
	{
		$hasil = $this->db->get('vw_untung_barang');
		return $hasil->result();
	}

}
