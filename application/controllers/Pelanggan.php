<?php
class Pelanggan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		// $level = 3;
		// $this->simple_login->cek_auth($level);
		$this->load->model('models');
		
	}

	public function index()
	{
		$this->load->view('pelanggan/headerpelanggan');
		$this->load->view('pelanggan/home');
		$this->load->view('pelanggan/footerpelanggan');
	}

	public function ajaxhome()
	{
		$id = $this->session->userdata('user_idpelanggan');
		// print_r($id);
		$list = $this->models->viewdatapelanggan($id);
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			if ($item->statusbayar == null ) {
				$aksi = '<button class="btn btn-sm btn-warning pdftagihan"  data-kode="'.$item->idtransaksi.'" title="Cetak Taihan"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			} else {
				$aksi = '<button class="btn btn-sm btn-warning pdftagihan"  data-kode="'.$item->idtransaksi.'" title="Cetak Tagihan"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-primary pdfpembayaran"  data-kode="'.$item->idtransaksi.'" title="Cetak Pembayaran"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->idbilling;
			$row[] = $item->nmbulan;
			$row[] = $item->tahun;
			$row[] = $item->namapaket;
			$row[] = number_format($item->harga,0,',','.');
			$row[] = $item->tglpembayaran;
			$row[] = $aksi;
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function pdf($start)
	{
		// $start = $this->input->post('idtransaksi');
		$list = $this->models->edit_tagihan($start);
		// print_r($list);
		$output = array(
			'data' => $list
		);

		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8', 
			'format' => [210,330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$file_name = 'Tagihan';
		$html = $this->load->view('pdftagihan', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
		// $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
		// $mpdf->Output($file_name .'.pdf', \Mpdf\Output\Destination::INLINE);
	}

	public function pdfpembayaran($id)
	{
		// $start = $this->input->post('idtransaksi');
		$list = $this->models->edit_tagihan($id);
		$output = array(
			'data' => $list
		);

		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8', 
			'format' => [210,330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfpembayaran', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

}