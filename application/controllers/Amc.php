<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Amc extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('models');
		// $level = 1;
		// $this->simple_login->cek_auth($level);
	}

	public function index()
	{
		$data['data'] = $this->models->getBarangTerjual();
		$this->load->view('header');
		$this->load->view('home', $data);
		$this->load->view('footer');
	}

	public function chart()
	{
		$data = $this->models->grafik();
		echo json_encode($data);
	}
	public function chartuntung()
	{
		$data = $this->models->getUntungGlobal();
		echo json_encode($data);
	}

	public function chartkategori()
	{
		$data = $this->models->grafikkategori();
		echo json_encode($data);
	}

	public function chartlistterjual()
	{
		$data = $this->models->getBarangTerjual();
		echo json_encode($data);
	}

	public function kategori()
	{
		$this->load->view('header');
		$this->load->view('kategori');
		$this->load->view('footer');
	}

	public function ajaxkategori()
	{
		$list = $this->models->kategori();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namakategori;
			$row[] = '<button class="btn btn-sm btn-warning editkategori"  data-kode="' . $item->idkategori . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapuskategori"  data-kode="' . $item->idkategori . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editkategori()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_kategori($id);

		echo json_encode($list);
	}

	public function insertkategori()
	{
		$namaarea = $_POST['namakategori'];
		$data = array(
			'namakategori' => strtoupper($namaarea)
		);

		$list = $this->models->insert('kategori', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updatekategori()
	{
		$id = $_POST['idkategori'];
		$namakategori = $_POST['namakategori'];
		$data = array(
			'namakategori' => strtoupper($namakategori)
		);

		$where = array(
			'idkategori' => $id,
		);

		$list = $this->models->update('kategori', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletekategori()
	{
		$id = $_POST['idkategori'];
		$data = array('idkategori' => $id);
		$list = $this->models->delete('kategori', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	// Supplier

	public function supplier()
	{
		$this->load->view('header');
		$this->load->view('supplier');
		$this->load->view('footer');
	}

	public function ajaxsuplier()
	{
		$list = $this->models->supplier();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namasup;
			$row[] = $item->alamatsup;
			$row[] = $item->notelpsup;
			$row[] = '<button class="btn btn-sm btn-warning editsupplier"  data-kode="' . $item->idsup . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapussupplier"  data-kode="' . $item->idsup . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editsupplier()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_supplier($id);

		echo json_encode($list);
	}

	public function insertsupplier()
	{
		$namasup = $_POST['namasupplier'];
		$alamatsup = $_POST['alamat'];
		$notelp = $_POST['notelp'];
		$data = array(
			'namasup' => strtoupper($namasup),
			'alamatsup' => strtoupper($alamatsup),
			'notelpsup' => $notelp
		);

		$list = $this->models->insert('supplier', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updatesupplier()
	{
		$id = $_POST['idsupplier'];
		$namasupplier = $_POST['namasupplier'];
		$alamat = $_POST['alamat'];
		$notelp = $_POST['notelp'];
		$data = array(
			'namasup' => strtoupper($namasupplier),
			'alamatsup' => strtoupper($alamat),
			'notelpsup' => $notelp
		);

		$where = array(
			'idsup' => $id,
		);

		$list = $this->models->update('supplier', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletesupplier()
	{
		$id = $_POST['id'];
		$data = array('idsup' => $id);
		$list = $this->models->delete('supplier', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function masuk()
	{
		$supplier = $this->models->supplier();
		$kategori = $this->models->kategori();
		$ekspedisi = $this->models->getDinas();
		$data = array(
			'supplier' => $supplier,
			'kategori' => $kategori,
			'ekspedisi' => $ekspedisi
		);
		$this->load->view('header');
		$this->load->view('masuk', $data);
		$this->load->view('footer');
	}

	public function ajaxmasuk()
	{
		$list = $this->models->masuk();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->kodebarang;
			$row[] = $item->namabarang;
			$row[] = $item->serialnumber;
			$row[] = $item->stok;
			$row[] = $item->tglmasuk;
			$row[] = $item->namasup;
			$row[] = $item->garansi;
			$row[] = '<button class="btn btn-sm btn-warning editmasuk"  data-kode="' . $item->idbarang . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapusmasuk"  data-kode="' . $item->idbarang . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editmasuk()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_masukid($id);

		echo json_encode($list);
	}

	public function insertmasuk()
	{
		$kodebarang = $_POST['kodebarang'];
		$namabarang = $_POST['namabarang'];
		$jumlah = $_POST['jumlah'];
		$serial = $_POST['serial'];
		$tglmasuk = $_POST['tglmasuk'];
		$supplier = $_POST['supplier'];
		$merek = $_POST['merek'];
		$kategori = $_POST['kategori'];
		$hargabeli = $_POST['hargabeli'];
		$garansi = $_POST['garansi'];
		$ekspedisi = $_POST['ekspedisi'];

		$data = array(
			'kodebarang' => $kodebarang,
			'namabarang' => strtoupper($namabarang),
			'serialnumber' => $serial,
			'merek' => strtoupper($merek),
			'stokawal' => $jumlah,
			'stok' => $jumlah,
			'tglmasuk' => $tglmasuk,
			'idsupp' => $supplier,
			'idkategori' => $kategori,
			'hargabeli' => $hargabeli,
			'garansi' => $garansi,
			'iddinas' => $ekspedisi
		);


		$list = $this->models->insert('barang', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updatemasuk()
	{
		$id = $_POST['idbarang'];
		$kodebarang = $_POST['kodebarang'];
		$namabarang = $_POST['namabarang'];
		$jumlah = $_POST['jumlah'];
		$serial = $_POST['serial'];
		$tglmasuk = $_POST['tglmasuk'];
		$supplier = $_POST['supplier'];
		$merek = $_POST['merek'];
		$kategori = $_POST['kategori'];
		$hargabeli = $_POST['hargabeli'];
		$garansi = $_POST['garansi'];
		$ekspedisi = $_POST['ekspedisi'];

		$data = array(
			'kodebarang' => $kodebarang,
			'namabarang' => strtoupper($namabarang),
			'serialnumber' => $serial,
			'merek' => strtoupper($merek),
			'stok' => $jumlah,
			'tglmasuk' => $tglmasuk,
			'idsupp' => $supplier,
			'idkategori' => $kategori,
			'hargabeli' => $hargabeli,
			'garansi' => $garansi,
			'iddinas' => $ekspedisi
		);

		$where = array(
			'idbarang' => $id,
		);

		$list = $this->models->update('barang', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletemasuk()
	{
		$id = $_POST['idbarang'];
		$data = array('idbarang' => $id);
		$list = $this->models->delete('barang', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function pengguna()
	{
		$list['data'] = $this->models->level();
		$this->load->view('header');
		$this->load->view('pengguna', $list);
		$this->load->view('footer');
	}

	public function ajaxpengguna()
	{
		$list = $this->models->pengguna();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->nama;
			$row[] = $item->username;
			$row[] = $item->namalevel;
			$row[] = '<button class="btn btn-sm btn-warning editpengguna"  data-kode="' . $item->idusers . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapuspengguna"  data-kode="' . $item->idusers . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editpengguna()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_pengguna($id);

		echo json_encode($list);
	}

	public function insertpengguna()
	{
		$namapengguna = $_POST['namapengguna'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$level = $_POST['level'];
		$data = array(
			'nama' => strtoupper($namapengguna),
			'username' => $username,
			'password' => md5($password),
			'level' => $level
		);

		$list = $this->models->insert('users', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updatepengguna()
	{
		$id = $_POST['idpengguna'];
		$namapengguna = $_POST['editnamapengguna'];
		$username = $_POST['editusername'];
		$password = $_POST['editpassword'];
		$level = $_POST['editlevel'];
		$data = array(
			'nama' => strtoupper($namapengguna),
			'username' => $username,
			'password' => md5($password),
			'level' => $level
		);

		$where = array(
			'idusers' => $id,
		);

		$list = $this->models->update('users', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletepengguna()
	{
		$id = $_POST['idpengguna'];
		$data = array('idusers' => $id);
		$list = $this->models->delete('users', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function keluar()
	{
		$barang = $this->models->masuk();
		// $paket = $this->models->paket();
		$data = array(
			"barang" => $barang
		);
		$this->load->view('header');
		$this->load->view('keluar', $data);
		$this->load->view('footer');
	}

	public function ajaxkeluar()
	{
		$list = $this->models->keluar();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			if ($this->session->userdata('user_level') == 1 && $item->sisa != 0) {
				$aksi = '<button class="btn btn-sm btn-primary pdfkeluar" title="Barang Keluar" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-warning pdfpembelian" title="Pembayaran" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-info pdfangsuran" title="Angsuran" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-success detailkeluar"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-search"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
				<button class="btn btn-sm btn-info bayar" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-money-bill-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-secondary detailangsuran"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-expand-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-danger hapuskeluar"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			} elseif ($item->sisa == 0 && $this->session->userdata('user_level') == 1) {
				$aksi = '<button class="btn btn-sm btn-primary pdfkeluar" title="Barang Keluar"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-warning pdfpembelian" title="Pembayaran" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-info pdfangsuran" title="Angsuran" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-success detailkeluar"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-search"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-secondary detailangsuran"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-expand-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-danger hapuskeluar"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			} elseif ($item->sisa == 0) {
				$aksi = '<button class="btn btn-sm btn-primary pdfkeluar" title="Barang Keluar" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-warning pdfpembelian" title="Pembayaran" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-info pdfangsuran" title="Angsuran" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-success detailkeluar"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-search"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-secondary detailangsuran"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-expand-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-danger hapuskeluar"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			} else {
				$aksi = '<button class="btn btn-sm btn-primary pdfkeluar" title="Barang Keluar" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-warning pdfpembelian" title="Pembayaran" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-info pdfangsuran" title="Angsuran" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-file-pdf"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-success detailkeluar"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-search"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
				<button class="btn btn-sm btn-info bayar" data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-money-bill-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-secondary detailangsuran"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-expand-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-danger hapuskeluar"  data-kode="' . $item->idtransaksi . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			}

			if ($item->sisa == 0) {
				$sisa = '<span class="badge badge-warning ">LUNAS</span>';
			} else {
				$sisa = '<span class="badge badge-danger ">Belum LUNAS</span>';
				// $sisa = '<span class="badge badge-danger ">'.number_format($item->sisa,0,',','.').'</span>';
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->idtransaksi;
			$row[] = $item->namapembeli;
			$row[] = $item->stokkeluar;
			$row[] = number_format($item->totalharga, 0, ',', '.');
			$row[] = $item->tglkeluar;
			$row[] = number_format($item->bayar, 0, ',', '.');
			$row[] = $sisa;
			$row[] = $aksi;
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function deletekeluar()
	{
		$id = $_POST['idtrans'];
		$data = array('idtransaksi' => $id);
		$list = $this->models->delete('trans', $data);
		$list = $this->models->delete('transaksi', $data);
		$list = $this->models->delete('angsuran', $data);
		// $dataup = array(
		// 	'tglhapus' => date('Y-m-d'),
		// 	'userhapus' => $this->session->userdata('id_user')
		// );

		// $list = $this->models->update('transaksi', $dataup, $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function get_pembayaran()
	{
		$id = $this->input->post('id');
		$list = $this->models->sumget_onekeluar($id);

		echo json_encode($list);
	}

	public function updatepembayaran()
	{
		$id = $_POST['id'];
		$bayar = $_POST['bayar'];
		$sisa = $_POST['sisa'];
		$list = $this->models->get_onebayar($id);
		$ss = $list[0]['bayar'];
		$byr = $ss + $bayar;
		// print_r($ss);


		$ang = array(
			'idtransaksi' => $id,
			'angsuran' => $bayar,
			'tglangsuran' => date('Y-m-d'),
			'user' => $this->session->userdata('id_user')
		);

		$this->db->insert('angsuran', $ang);

		$data = array(
			'bayar' => $byr,
			'sisa' => $sisa,
			'tglbayar' => date('Y-m-d')
		);

		$where = array(
			'idtransaksi' => $id,
		);



		$list = $this->models->update('trans', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function vwkeluar()
	{
		$id = $_POST['id'];
		$paket = $this->models->edit_masukid($id);

		echo json_encode($paket);
	}

	// public function insertkeluar()
	// {
	// 	$idkategori = $_POST['idkategori'];
	// 	$idbarang = $_POST['idbarang'];
	// 	$tglkeluar = $_POST['tglkeluar'];
	// 	$jmlkeluar = $_POST['jmlkeluar'];
	// 	$nama = $_POST['nama'];
	// 	$data = array(
	// 		'idkategori' => $idkategori,
	// 		'idbarang' => $idbarang,
	// 		'tglkeluar' => $tglkeluar,
	// 		'jmlkeluar' => $jmlkeluar,
	// 		'namapengambil' => $nama
	// 	);

	// 	$list = $this->models->insert('transaksi', $data);

	// 	if ($list) {
	// 		echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
	// 	} else {
	// 		echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
	// 	}

	// }

	public function indexkeluar()
	{
		$barang = $this->models->getbarang();
		// $paket = $this->models->paket();
		$data = array(
			"barang" => $barang
		);
		$this->load->view('header');
		$this->load->view('inputkeluar', $data);
		$this->load->view('footer');
	}

	public function simpan()
	{
		$master = $this->input->post('master');
		$detail = $this->input->post('detail');
		$idbarang = $this->input->post('idbarang');
		$idkategori = $this->input->post('idkategori');
		$jumlah = $this->input->post('jumlah');
		$harga = $this->input->post('hargajual');
		$ket = $this->input->post('ket');
		$total = $this->input->post('total');
		$bayar = $this->input->post('bayar');
		$sisa = $this->input->post('sisa');
		$namapengambil = $this->input->post('namapengambil');
		$tglpenjualan = $this->input->post('tgljual');

		$data = array(
			'namapembeli' => strtoupper($namapengambil),
			'tglkeluar' => $tglpenjualan,
			'iduser' => $this->session->userdata('id_user'),
			'total' => $total,
			'bayar' => $bayar,
			'sisa' => $sisa,
			'tglbayar' => date('Y-m-d'),
			'tglentry' => date('Y-m-d')

		);

		$this->db->insert('trans', $data);

		$arr_new = array(
			'idbarang' => $idbarang,
			'idkategori' => $idkategori,
			'jumlah' => $jumlah,
			'hargajual' => $harga,
			'keterangan' => $ket
		);

		/*sesuaikan dengan format arrau*/
		$kode = $this->models->maxid();

		$ang = array(
			'idtransaksi' => $kode[0]['idtransaksi'],
			'angsuran' => $bayar,
			'tglangsuran' => date('Y-m-d'),
			'user' => $this->session->userdata('id_user')
		);

		$this->db->insert('angsuran', $ang);
		$cart = [];

		for ($col = 0; $col < count($arr_new['idkategori']); $col++) {

			$data = array(
				'idtransaksi' => $kode[0]['idtransaksi'],
				'idkategori' => $arr_new['idkategori'][$col],
				'idbarang'	=> $arr_new['idbarang'][$col],
				'hargajual'	=> $arr_new['hargajual'][$col],
				'jmlkeluar'	=> $arr_new['jumlah'][$col],
				'keterangan' => $arr_new['keterangan'][$col],

			);

			array_push($cart, $data);
		}

		$list = $this->models->multiSave('transaksi', $cart);

		if ($list == TRUE) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function detailpembeliam()
	{
		$kode = $this->input->post('kode');
		$list = $this->models->get_detailkeluar($kode);
		$data = array();
		$no = 0;
		foreach ($list as $item) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->kodebarang;
			$row[] = $item->namabarang;
			$row[] = $item->serialnumber;
			$row[] = $item->merek;
			$row[] = $item->jmlkeluar;
			$row[] = number_format($item->hargajual, 0, ',', '.');
			$row[] = $item->keterangan;
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
			"kode" => $kode
		);
		echo json_encode($output);
	}

	public function detailangsuran()
	{
		$kode = $this->input->post('kode');
		$list = $this->models->get_detailangsuran($kode);
		$data = array();
		$no = 0;
		foreach ($list as $item) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->idtransaksi;
			$row[] = number_format($item->angsuran, 0, ',', '.');
			$row[] = $item->tglangsuran;
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
			"kode" => $kode
		);
		echo json_encode($output);
	}



	public function get_edittagihan()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_tagihan($id);

		echo json_encode($list);
	}


	public function laporan()
	{
		$kategori  = $this->models->kategori();
		// $barang  = $this->models->edit_masukid();
		$data = array(
			'data' => $kategori
		);
		$this->load->view('header');
		$this->load->view('laporan', $data);
		$this->load->view('footer');
	}

	public function getbrgkat()
	{
		$id = $_POST['id'];
		$paket = $this->models->baranglap($id);

		echo json_encode($paket);
	}


	public function pdf($start)
	{
		// $start = $this->input->post('idtransaksi');
		$list = $this->models->edit_tagihan($start);
		// print_r($list);
		$output = array(
			'data' => $list
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$file_name = 'Tagihan';
		$html = $this->load->view('pdftagihan', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
		// $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY);
		// $mpdf->Output($file_name .'.pdf', \Mpdf\Output\Destination::INLINE);
	}

	public function pdfangsuran($id)
	{
		$detail = $this->models->get_detailangsuran($id);
		$dus  = $this->models->get_onekeluar($id);
		$total  = $this->models->sumget_onekeluar($id);
		$output = array(
			'dus' => $dus,
			'detail' => $detail,
			'total' => $total
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$file_name = 'Tagihan';
		$html = $this->load->view('pdfangsuran', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfpembayaran($id)
	{
		$list = $this->models->edit_tagihan($id);
		$output = array(
			'data' => $list
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfpembayaran', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfmasuklaporan()
	{
		$start = $this->input->post('datepicker');
		$end = $this->input->post('datepicker1');
		$list = $this->models->cetak_masuk($start, $end);
		$sum = $this->models->sum_cetak_masuk($start, $end);
		$output = array(
			'data' => $list,
			'sum' => $sum
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfmasuk', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfkeluarlaporan()
	{
		$start = $this->input->post('datepicker2');
		$end = $this->input->post('datepicker3');
		$list = $this->models->cetak_keluar($start, $end);
		$sum = $this->models->sum_cetak_keluar($start, $end);
		$output = array(
			'data' => $list,
			'sum' => $sum
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfkeluar', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfstokbarang()
	{
		$start = $this->input->post('lapkategori');
		$end = $this->input->post('lapbarang');

		if ($start == '' && $end == '') {
			$list = $this->models->barang();
			$sum = $this->models->sumstokbrg();
		} elseif ($end == '') {
			$list = $this->models->barangkategori($start);
			$sum = $this->models->getsumbarangkat($start);
		} else {
			$list = $this->models->getbarangkategori($start, $end);
			$sum = $this->models->getsumbarangkategori($start, $end);
		}

		$output = array(
			'data' => $list,
			'sum' => $sum
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfstokbarang', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfpembelian($id)
	{
		// $kode = $this->input->post('kode');
		$list = $this->models->get_detailkeluar($id);
		$dus  = $this->models->get_onekeluar($id);
		$total  = $this->models->sumget_onekeluar($id);

		$output = array(
			'data' => $list,
			'dus' => $dus,
			'total' => $total
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfpembelian', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfpengeluaran($id)
	{
		$list = $this->models->get_detailkeluar($id);
		$dus  = $this->models->get_onekeluar($id);

		$output = array(
			'data' => $list,
			'dus' => $dus
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfpengeluaran', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function pdfrekaplaba()
	{

		$list = $this->models->getRekapUntung();
		$sum = $this->models->getUntungGlobal();

		$output = array(
			'data' => $list,
			'sum' => $sum
		);

		$mpdf = new \Mpdf\Mpdf([
			'tempDir' => '/tmp',
			'mode' => 'utf-8',
			'format' => [210, 330],
			'orientation' => 'L',
			'margin_left' => 5,
			'margin_right' => 5,
			'margin_top' => 10,
			'margin_bottom' => 2,
			'margin_header' => 0,
			'margin_footer' => 0,
		]);
		$html = $this->load->view('pdfrekaplaba', $output, TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function dinas()
	{
		$this->load->view('header');
		$this->load->view('dinas');
		$this->load->view('footer');
	}

	public function ajaxdinas()
	{
		$list = $this->models->getDinas();
		$data = array();
		$no = 0;
		foreach ($list as $item) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->tujuan;
			$row[] = $item->keperluan;
			$row[] = $item->tanggal;
			$row[] = '<button class="btn btn-sm btn-primary ekspedisi" title="Barang Keluar" data-kode="' . $item->id . '"><i class="nav-icon fas fa-plus-circle"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				<button class="btn btn-sm btn-success detaildinas"  data-kode="' . $item->id . '"><i class="nav-icon fas fa-search"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
				<button class="btn btn-sm btn-warning editdinas"  data-kode="' . $item->id . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
				<button class="btn btn-sm btn-danger hapusdinas"  data-kode="' . $item->id . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data
		);
		echo json_encode($output);
	}

	public function dinasdetail()
	{
		$id   =	$_POST['kode'];
		$list = $this->models->getDinasDetail($id);
		$data = array();
		$no = 0;
		foreach ($list as $item) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->ekspedisi;
			$row[] = $item->tujuan;
			$row[] = $item->hargapengeluaran;
			$row[] = $item->tanggal;
			$row[] = '<button class="btn btn-sm btn-warning editpengguna"  data-kode="' . $item->id . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
				<button class="btn btn-sm btn-danger hapuspengguna"  data-kode="' . $item->id . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>
				';
			$data[] = $row;
		}

		$output = array(
			"data" => $data
		);
		echo json_encode($output);
	}

	public function insertdinas()
	{
		$tgl = $_POST['tgldinas'];
		$tujuan = $_POST['tujuan'];
		$keperluan = $_POST['keperluan'];
		$data = array(
			'tanggal' => $tgl,
			'tujuan' => $tujuan,
			'keperluan' => $keperluan
		);

		$list = $this->models->insert('dinas', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function get_editdinas()
	{
		$id = $this->input->post('id');
		$list = $this->models->edit_dinas($id);

		echo json_encode($list);
	}


	public function updatedinas()
	{
		$id = $_POST['id'];
		$tgl = $_POST['tgldinas'];
		$tujuan = $_POST['tujuan'];
		$keperluan = $_POST['keperluan'];
		$data = array(
			'tanggal' => $tgl,
			'tujuan' => $tujuan,
			'keperluan' => $keperluan
		);

		$where = array(
			'id' => $id,
		);

		$list = $this->models->update('dinas', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Update', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deletedinas()
	{
		$id = $_POST['id'];
		$data = array('id' => $id);
		$list = $this->models->delete('dinas', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function simpanekspedisi()
	{

		$id = $this->input->post('id');
		$ekspedisi = $this->input->post('ekspedisi');
		$tujuan = $this->input->post('tujuan');
		$harga = $this->input->post('harga');
		$tanggal = $this->input->post('tanggal');

		$arr_new = array(
			'iddinas' => $id,
			'tanggal' => $tanggal,
			'tujuan' => $tujuan,
			'ekspedisi' => $ekspedisi,
			'hargapengeluaran' => $harga

		);

		$cart = [];

		for ($col = 0; $col < count($arr_new['iddinas']); $col++) {

			$data = array(
				'iddinas' => $arr_new['iddinas'][$col],
				'tanggal' => $arr_new['tanggal'][$col],
				'tujuan'	=> $arr_new['tujuan'][$col],
				'hargapengeluaran'	=> $arr_new['hargapengeluaran'][$col],
				'ekspedisi'	=> $arr_new['ekspedisi'][$col],

			);

			array_push($cart, $data);
		}

		$list = $this->models->multiSave('dinas_detail', $cart);

		if ($list == TRUE) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function rak()
	{
		$this->load->view('header');
		$this->load->view('rak');
		$this->load->view('footer');
	}

	public function ajaxrak()
	{
		$list = $this->models->rak();
		$data = array();
		$no = 0;
		foreach ($list as $item) {

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->namarak;
			$row[] = '<button class="btn btn-sm btn-warning editrak"  data-kode="' . $item->id . '"><i class="nav-icon fas fa-pencil-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div> 
			<button class="btn btn-sm btn-danger hapusrak"  data-kode="' . $item->id . '"><i class="nav-icon fas fa-trash-alt"></i></button> <div id="process" class="spinner-border text-warning" role="status" style="display: none;"><span class="sr-only"></span></div>';
			$data[] = $row;
		}

		$output = array(
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_editrak()
	{
		$id = $this->input->post('idrak');
		$list = $this->models->edit_rak($id);

		echo json_encode($list);
	}

	public function insertrak()
	{
		$namarak = $_POST['namarak'];
		$data = array(
			'namarak' => strtoupper($namarak)
		);

		$list = $this->models->insert('rak', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function updaterak()
	{
		$id = $_POST['idrak'];
		$namarak = $_POST['namarak'];
		$data = array(
			'namarak' => strtoupper($namarak)
		);

		$where = array(
			'id' => $id,
		);

		$list = $this->models->update('rak', $data, $where);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Simpan', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

	public function deleterak()
	{
		$id = $_POST['idrak'];
		$data = array('id' => $id);
		$list = $this->models->delete('rak', $data);

		if ($list) {
			echo json_encode(array("status" => TRUE, "alert" => 'success', "data" => 'Berhasil Dihapus', "title" => 'Info'));
		} else {
			echo json_encode(array("status" => TRUE, "alert" => 'warning', "data" => 'Tidak Berhasil', "title" => 'Info'));
		}
	}

}
