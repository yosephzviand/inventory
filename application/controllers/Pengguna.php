<?php


class Pengguna extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function index()
	{
		$this->load->library('form_validation');
		$valid = $this->form_validation;
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->load->view('login');

		$valid->set_rules('username', 'Username', 'required');
		$valid->set_rules('password', 'Password', 'required');

		if ($valid->run()) {
			$this->simple_login->login($username, $password);
		} 
	}

	public function logout() {

		$this->simple_login->logout();
	}
}
