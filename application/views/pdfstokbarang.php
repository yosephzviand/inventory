<table border="">
	<tr>
		<td rowspan="" style="text-align: center;" width="200px"><img src="<?php echo base_url() ?>assets/dist/img/gesitlogokecil.PNG" width="120px" alt="AdminLTE Logo"></td>
		<td>
			<td>
				<p style="font-size: 18px"><b>GEKA SOLUSI UTAMA</b></p>
				<p style="font-size: 12px">Jalan Yogya-Wonosari Km. 17,5 Patuk Gunungkidul Yogyakarta 55862</p>
				<p style="font-size: 12px">Telp  : 085225225959 (CS) Email : atomedia_mail@yahoo.com</p>
			</td>
		</tr>
	</table>
	<hr>

	<h3 style="text-align: center;">Data Stok Barang </h3>
	<table width="100%" cellspacing="0" cellpadding="3" style="font-size: 9pt;" border="1">
		<thead>
			<tr>
				<th>No</th>
				<th>Kategori</th>
				<th>Kode Barang</th>
				<th>Nama Barang</th>
				<th>Serial</th>
				<th>Jumlah Stok</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1; foreach ($data as $key) : 
			// $total = $total + $key->stok;
			?>
			<tr>
				<td style="text-align: center;"><?= $no++ ?></td>
				<td><?= $key->namakategori ?></td>
				<td><?= $key->kodebarang ?></td>
				<td><?= $key->namabarang ?></td>
				<td><?= $key->serialnumber ?></td>
				<td style="text-align: right;"><?= $key->stok ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<?php
		$no = 1; foreach ($sum as $key) :
		?>
		<tr>
			<td colspan="5" style="text-align: center;"><b>TOTAL</b></td>
			<td style="text-align: right;"><?= $key->total ?></td>
		</tr>
	<?php endforeach; ?>
</tfoot>
</table>