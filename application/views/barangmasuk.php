<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button type="button" id="pdfdesa" class="btn btn-primary" data-toggle="modal" data-target="#modal-paket" title="Tambah Data Area" > Tambah
            </button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table width="100%" id="tbmasuk" class="table table-striped table-bordered table-sm">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang </th>
                    <th>Serial</th>
                    <th>Jumlah</th>
                    <th>Tgl. Masuk</th>
                    <th>Supplier</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modal-paket">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Supplier</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6 col-12">
                
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary simpan_supplier">Simpan</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-edit-supplier">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Supplier</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">ID</label>
              <div class="col-sm-10">
                <input type="text" name="idsupplier" class="form-control" id="idsupplier" placeholder="ID" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Supplier</label>
              <div class="col-sm-10">
                <input type="text" name="editnamasupplier" class="form-control" id="editnamasupplier" placeholder="Nama Area">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">Alamat</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="editalamat" name="editalamat" placeholder="Harga">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">No. Telp</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="editnotelp" name="editnotelp" placeholder="Harga">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary simpan_supplier_edit">Simpan</button>
      </div>
    </div>
  </div>
</div>