
<!-- Main Footer -->
<footer class="main-footer" style="text-align: center;">
  <!-- Default to the left -->
  <strong>Copyright &copy; 2021 <a href="https://mervia.my.id">MERVIA</a>.</strong> All rights reserved.
</footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/dist/js/adminlte.min.js"></script>
<script>
  // $.widget.bridge('uibutton', $.ui.button)
  var site_url = '<?php echo site_url(); ?>';
</script>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/dist/js/pelanggan.js"></script>
</body>
</html>
