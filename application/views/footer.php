</div>

<footer class="main-footer">
	<strong>Copyright &copy; 2021 <a href="http://mervia.my.id" target="_blank">MERVIA</a>.</strong>
	All rights reserved.
</footer>

</div>

<!-- jQuery -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="<?php echo base_url('assets/js'); ?>/toastr.min.js"></script>
<script>
	$.widget.bridge('uibutton', $.ui.button)
	var site_url = '<?php echo site_url(); ?>';
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/chart.js/Chart.min.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/dist/js/adminlte.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?php echo base_url() ?>assets/AdminLTE3/dist/js/demo.js"></script> -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/dist/js/masadi.js"></script>

<script src="<?php echo base_url() ?>assets/assets/js/shared/misc.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/select2/js/select2.full.min.js"></script>
<!-- Highchart -->
<script src="<?= base_url() ?>assets/Highcharts/code/highcharts.js"></script>
<!-- <script src="<?= base_url() ?>assets/Highcharts/code/modules/variable-pie.js"></script>
<script src="<?= base_url() ?>assets/Highcharts/code/modules/accessibility.js"></script> -->
<!-- bootstrap datepicker -->
<script src="<?= base_url()?>assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- jquery-validation -->
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>assets/AdminLTE3/plugins/jquery-validation/additional-methods.min.js"></script>

<script src="<?php echo base_url()?>assets/sweetalert/dist/sweetalert.min.js"></script>
<script>
	$(function () {

    $('.select2').select2({
    	theme: 'bootstrap4'
    })


    $('#barangkeluar').change(function () {
      var id = $(this).val();
      $.ajax({
        url: site_url + "amc/vwkeluar",
        method: "POST",
        data: { id: id },
        async: true,
        dataType: 'json',
        success: function (data) {
          for (i in data) {
            $('[name="stok"]').val(data[i].stok);
          }
        }
      });
      return false;
    });

    $('#lapkategori').change(function () {
      var id = $(this).val();
      console.log(id);
      $.ajax({
        url: site_url + "amc/getbrgkat",
        method: "POST",
        data: { id: id },
        async: true,
        dataType: 'json',
        success: function (data) {
          console.log(data);
          var html = '';
          var i;
          html = '<option value="">Pilih Semua Barang</option>';
          for (i = 0; i < data.length; i++) {
            html += '<option value=' + data[i].kodebarang + '>' + data[i].namabarang + '</option>';
          }
          $('#lapbarang').html(html);
        }
      });
      return false;
    });

    $('#formMasuk').validate({
      rules: {
        masukkategori: {
          required: true,
          masukkategori: true,
        },
        kodebarang: {
          required: true,
        },
        namabarang: {
          required: true
        },
        serial: {
          required: true
        },
        merek: {
          required: true
        },
        datepicker: {
          required: true
        },
        jumlah: {
          required: true
        },
        merek: {
          masuksupplier: true
        },
      },
      messages: {
        masukkategori: {
          required: "Please enter a email address"
        },
        kodebarang: {
          required: "Please provide a password",
          minlength: "Your password must be at least 5 characters long"
        },
        namabarang: "Please accept our terms"
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
      }
    });


  });

  function rubah(angka) {
    var reverse = angka.toString().split('').reverse().join(''),
    ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join('.').split('').reverse().join('');
    return ribuan;
  }

  function sum() {
    var harga = document.getElementById('tagihan').value;
    var tambahan = document.getElementById('tambahan').value;
    var result =  parseInt(harga) + parseInt(tambahan);
    if (!isNaN(result)) {
     document.getElementById('totaltagihan').value = result;
   } 
 }

 function sumedit() {
  var harga = document.getElementById('edittagihan').value;
  var tambahan = document.getElementById('edittambahan').value;
  var result =  parseInt(harga) + parseInt(tambahan);
  if (!isNaN(result)) {
   document.getElementById('edittotaltagihan').value = result;
 }
}

 function sumbayar() {
  var harga = document.getElementById('totalkeluar').value;
  var tambahan = document.getElementById('bayarkeluar').value;
  var result =  parseInt(harga) - parseInt(tambahan);
  if (!isNaN(result)) {
   document.getElementById('sisakeluar').value = result;
 }
}

function sumbayarangs() {
  var harga = document.getElementById('kurang').value;
  var tambahan = document.getElementById('bayar').value;
  var result =  parseInt(harga) - parseInt(tambahan);
  if (!isNaN(result)) {
   document.getElementById('sisa').value = result;
 }
}


</script>

</body>
</html>
