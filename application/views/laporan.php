<section class="content">
	<div class="container-fluid">

		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Pembukuan dan Pelaporan</h3>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				<div id="accordion">
					<!-- we are adding the .class so bootstrap.js collapse plugin detects it -->
					<div class="card card-primary">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									Cetak Stok Barang
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="card-body">
								<form name="ipladder2" id="ipladder2" method="post">
									<div class="form-group row">
										<div class="col-sm-4 col-6">
											<select class="form-control select2" name="lapkategori" id="lapkategori">
												<option value="">Pilih Semua Kategori</option>
												<?php
												foreach ($data as $key) {
												?>
													<option value="<?php echo $key->idkategori; ?>"><?php echo $key->namakategori; ?></option>
												<?php
												}
												?>
											</select>
										</div>
										<div class="col-sm-4 col-6">
											<select class="form-control select2" name="lapbarang" id="lapbarang">
												<option value="">Pilih Semua Barang</option>
											</select>
										</div>

										<div class="col-sm-4 col-6">
											<button type="button" id="cetakstokbrg" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
												<i class="fas fa-file-pdf"></i> PDF
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="card card-danger">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									Cetak Barang Keluar
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							<div class="card-body">
								<form name="ipladder1" id="ipladder1" method="post">
									<div class="form-group row">
										<div class="col-sm-3 col-6">
											<div class="input-group date">
												<input name="datepicker2" type="text" class="form-control pull-right" id="datepicker2" placeholder="Masukkan Tanggal" required="true">
											</div>
										</div>
										<label class="col-form-label"> s/d</label>
										<div class="col-sm-3 col-6">
											<div class="input-group date">
												<input name="datepicker3" type="text" class="form-control pull-right" id="datepicker3" placeholder="Masukkan Tanggal" required="true">
											</div>
										</div>

										<button type="button" id="lappdfkeluar" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
											<i class="fas fa-file-pdf"></i> PDF
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="card card-success">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									Cetak Barang Masuk
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse">
							<div class="card-body">
								<form name="ipladder" id="ipladder" method="post">
									<div class="form-group row">
										<div class="col-sm-3 col-6">
											<div class="input-group date">
												<input name="datepicker" type="text" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" required="true">
											</div>
										</div>
										<label class="col-form-label"> s/d</label>
										<div class="col-sm-3 col-6">
											<div class="input-group date">
												<input name="datepicker1" type="text" class="form-control pull-right" id="datepicker1" placeholder="Masukkan Tanggal" required="true">
											</div>
										</div>

										<button type="button" id="lappdfmasuk" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
											<i class="fas fa-file-pdf"></i> PDF
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>

					<div class="card card-warning">
						<div class="card-header">
							<h4 class="card-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
									Cetak Rekap Laba dan Barang Terjual
								</a>
							</h4>
						</div>
						<div id="collapseFour" class="panel-collapse collapse">
							<div class="card-body">
								<form name="formlaba" id="formlaba" method="post">
									<div class="form-group row">

										<div class="col-sm-4 col-6">
											<button type="button" id="laprekaplaba" class="btn btn-primary" data-card-widget="Filter" data-toggle="tooltip">
												<i class="fas fa-file-pdf"></i> PDF
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
