<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-inp-dinas" title="Tambah Perjalanan Dinas"> Tambah
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table width="100%" id="tbdinas" class="table table-striped table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tujuan</th>
                                        <th>Keperluan</th>
                                        <th>Tanggal</th>
                                        <!-- <th>Total Harga</th> -->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-inp-dinas">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Perjalanan Dinas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Tanggal Dinas</label>
                            <div class="col-sm-8">
                                <input type="text" name="tgldinas" class="form-control" id="datepicker" placeholder="Tanggal Dinas">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Tujuan</label>
                            <div class="col-sm-8">
                                <input type="text" name="tujuan" class="form-control" id="tujuan" placeholder="Tujuan">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Keperluan</label>
                            <div class="col-sm-8">
                                <input type="text" name="keperluan" class="form-control" id="keperluan" placeholder="Keperluan">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary simpan_dinas">Simpan</button>
            </div>
        </div>
        </form>
    </div>
</div>

<div class="modal fade" id="modal-edit-dinas">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Perjalanan Dinas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">ID</label>
                            <div class="col-sm-8">
                                <input type="text" name="iddinas" class="form-control" id="iddinas" placeholder="Tanggal Dinas" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Tanggal Dinas</label>
                            <div class="col-sm-8">
                                <input type="text" name="edittgldinas" class="form-control" id="datepicker1" placeholder="Tanggal Dinas">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Tujuan</label>
                            <div class="col-sm-8">
                                <input type="text" name="edittujuan" class="form-control" id="edittujuan" placeholder="Tujuan">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Keperluan</label>
                            <div class="col-sm-8">
                                <input type="text" name="editkeperluan" class="form-control" id="editkeperluan" placeholder="Keperluan">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary simpan_edit_dinas">Simpan</button>
            </div>
        </div>
        </form>
    </div>
</div>

<div class="modal fade" id="modaldetail" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Detail</h3>
            </div>
            <div class="modal-body form">
                <div class="container-fluid">
                    <div class="table-responsive">
                        <table width="100%" id="detail_dinas" class="table table-striped table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Ekspedisi</th>
                                    <th>Tujuan</th>
                                    <th>Harga</th>
                                    <th>tanggal</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-inp-ekspedisi">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ekspedisi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <form id="pembelian" method="POST">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">ID</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="iddinas" class="form-control" id="iddinas" placeholder="Tanggal Dinas" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Tanggal Dinas</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="tgldinas" class="form-control" id="datepicker1" placeholder="Tanggal Dinas" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Tujuan</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="tujuan" class="form-control" id="tujuan" placeholder="Tujuan" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Keperluan</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="keperluan" class="form-control" id="keperluan" placeholder="Keperluan" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                                        <div class="col-sm-8">
                                            <button type="button" class="btn btn-primary simpan_ekspedisi">Tambah Barang</button>
                                        </div>
                                    </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Ekspedisi</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="ekspedisi" class="form-control" id="ekspedisi" placeholder="Ekspedisi">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Tujuan</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="tujuanekspedisi" class="form-control" id="tujuanekspedisi" placeholder="Tujuan Ekspedisi">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Harga</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="hargapengeluaran" class="form-control" id="hargapengeluaran" placeholder="Pengeluaran">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-4 col-form-label">Tanggal Ekspedisi</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="tglekspedisi" class="form-control" id="datepicker2" placeholder="Tanggal Dinas">
                                    </div>
                                </div>

                            </div>
                </form>
                <div class="table-responsive">
                    <table width="100%" class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <!-- <th>#</th> -->
                                <th>Ekspedisi</th>
                                <th>Tujuan </th>
                                <th>Harga</th>
                                <th>Tanggal</th>
                                <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody id="tb_ekspedisi">

                        </tbody>
                    </table>

                    <button class="btn btn-success float-right bt_simpan_ekspedisi" type="button"><i class="fa fa-save"></i> Simpan</button>
                </div>

            </div>
        </div>
        </form>
    </div>
</div>
</div>
</div>