<section class="content">
	<div class="container-fluid">
		<div class="row">

			<div class="col-md-8">
				<div class="card">
					<div class="card-body">
						<figure class="highcharts-container">
							<div id="container"></div>
						</figure>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-body">
						<label>Penjualan Terlaris Tahun <?= date('Y') ?></label>
						<div class="table-responsive">
							<table class="table table-sm table-stripped">
								<thead>
									<tr>
										<th>#</th>
										<th>Kategori</th>
										<th>Jumlah</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($data as $data) :
									?>
										<tr>
											<td><?= $no++ ?></td>
											<td><?= $data->kategori ?></td>
											<td><?= $data->jumlah ?></td>
										</tr>
									<?php
									endforeach;
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<div class="card">
					<div class="card-body">
						<figure class="highcharts-figure">
							<div id="container12"></div>
						</figure>
					</div>
				</div>
			</div>
			<div class="col-6">
				<div class="card">
					<div class="card-body">
						<figure class="highcharts-figure">
							<div id="container4"></div>
						</figure>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<figure class="highcharts-figure">
							<div id="container5"></div>
						</figure>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
