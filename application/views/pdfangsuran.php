<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		#customers {
			font-family: Arial, Helvetica, sans-serif;
			border-collapse: collapse;
			width: 100%;
		}

		#customers td, #customers th {
			border: 1px solid #ddd;
			padding: 8px;
		}

		#customers tr:nth-child(even){background-color: #f2f2f2;}

		#customers tr:hover {background-color: #ddd;}

		#customers th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
			background-color: #4CAF50;
			color: white;
		}
	</style>
</head>
<body style="background-image: url('assets/dist/img/pdfbg1.jpg');
background-position: top left;
background-repeat: no-repeat;
background-image-resize: 4;
background-image-resolution: from-image;">
<table>
	<tr>
		<td height="300px"></td>
	</tr>
</table>
<?php 
$no = 1;
foreach ($dus as $dus) : 
	?>

	<table width="100%"  cellspacing="0" cellpadding="3" style="font-size: 10pt;" border="">
		<tr>
			<td><b>Kepada : </b></td>
			<td width="210px"></td>
			<td width="90px"><b>Invoince</b></td>
			<td width="10px"><b> : </b></td>
			<td> <?= $dus->nomorsurat ?></td>
			<!-- <td colspan="3"></td> -->
		</tr>
		<tr>
			<td>Sdr. <?= $dus->namapembeli ?></td>
			<td></td>
			<td><b>Tanggal</b></td>
			<td><b> : </b></td>
			<td><?= $dus->tglkeluar ?></td>
			
		</tr>
		<tr>
			<td colspan="2"></td>
			<td><b>ID Billing</b></td>
			<td><b>:</b></td>
			<td><?= $dus->idtransaksi ?></td>
		</tr>
	</table>
	<br>
<?php endforeach; ?>


<table width="100%" id="customers" cellspacing="0" cellpadding="3" style="font-size: 10pt;" border="1">
	<thead >
		<tr>
			<th style="text-align: center;" width="10px" >No</th>
			<th style="text-align: center;">Tgl. Pembayaran</th>
			<th style="text-align: center;">Angsuran</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($detail as $key) : 
	// print_r($key);
			// $sub = $key->jmlkeluar * $key->hargajual;
			?>
			<tr>
				<td><?= $no++ ?></td>
				<td style="text-align: center;"><?= $key->tglangsuran?></td>
				<td style="text-align: right;"><?= number_format($key->angsuran,0,',','.') ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	
	<tfoot>
		<?php foreach ($total as $total ) : ?>
			
			<tr>
				<td colspan="2" style="text-align: center;"><b>Total Bayar</b></td>
				<td style="text-align: right;"> <b> Rp. <?=  number_format($total->bayar,0,',','.') ?></b></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;"><b>TOTAL Tagihan</b></td>
				<td style="text-align: right;"> <b> Rp. <?=  number_format($total->total,0,',','.') ?></b></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;"><b>Kurang Bayar</b></td>
				<td style="text-align: right;"> <b> Rp. <?=  number_format($total->sisa,0,',','.') ?></b></td>
			</tr>
		<?php endforeach; ?>
	</tfoot>
</table>
</body>
</html>