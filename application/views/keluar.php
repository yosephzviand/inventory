<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-inp-keluar" title="Tambah Barang Keluar" > Tambah
            </button>
            <!-- <button type="button" class="btn btn-primary btn-sm" onclick="tambah()" > Tambah
            </button> -->
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table width="100%" id="tbkeluar" class="table table-striped table-bordered table-sm">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>ID Penjualan</th>
                    <th>Nama Pembeli</th>
                    <th>Jumlah Barang</th>
                    <th>Total Harga</th>
                    <th>Tgl. Keluar</th>
                    <th>Bayar</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modal-pembayaran">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Pembayaran</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="form-group row">

              <label for="inputEmail3" class="col-sm-3 col-form-label">ID Transaksi</label>
              <div class="col-sm-9">
                <input type="text" name="idtransaksi" class="form-control" id="idtransaksi" placeholder="ID" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-3 col-form-label">Nama Pembeli</label>
              <div class="col-sm-9">
                <input type="text" name="namapembeli" class="form-control" id="namapembeli" placeholder="Tagihan" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-3 col-form-label">Tagihan</label>
              <div class="col-sm-9">
                <input type="text" name="total" class="form-control" id="total" placeholder="Bulan" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-3 col-form-label">Kurang Bayar</label>
              <div class="col-sm-9">
               <input type="text" name="kurang" class="form-control" id="kurang" placeholder="Paket" readonly="true" onkeyup="sumbayarangs()">
             </div>
           </div>
           <div class="form-group row">
            <label for="inputPassword3" class="col-sm-3 col-form-label">Pembayaran</label>
            <div class="col-sm-9">
              <input type="text" name="bayar" class="form-control" id="bayar" placeholder="Pembayaran" onkeyup="sumbayarangs()" >
            </div>
          </div>
          <div class="form-group row">
            <label for="inputPassword3" class="col-sm-3 col-form-label">Sisa</label>
            <div class="col-sm-9">
              <input type="text" name="sisa" class="form-control" id="sisa" placeholder="Pembayaran" onkeyup="sumbayarangs()" >
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="modal-footer justify-content-between">
      <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      <button type="button" class="btn btn-primary simpan_pembayaran">Bayar</button>
    </div>
  </div>
</div>
</div>


<div class="modal fade" id="modal-inp-keluar">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Barang Keluar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="row">               
              <div class="col-lg-6 col-12">
                <form  id="pembelian" method="POST">
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Tanggal Penjualan</label>
                    <div class="col-sm-8">
                      <input type="text" name="tglpenjualan" class="form-control" id="datepicker" placeholder="Tanggal Keluar" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Barang</label>
                    <div class="col-sm-8">
                      <select class="form-control select2" name="barangkeluar" id="barangkeluar" style="width: 100%;">
                        <option value="" >Pilih Barang</option>
                        <?php foreach ($barang as $key) : ?>
                          <option value="<?php echo $key->idbarang; ?>"><?php echo $key->namabarang; ?> (<?= $key->serialnumber ?>)</option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Stok</label>
                    <div class="col-sm-8">
                      <input type="text" name="stok" class="form-control" id="stok" placeholder="Stok" readonly="">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                    <div class="col-sm-8">
                      <button type="button" class="btn btn-primary simpan_keluar">Tambah Barang</button>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-12">
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Nama Pengambil</label>
                    <div class="col-sm-8">
                      <input type="text" name="namapengambil" class="form-control" id="namapengambil" placeholder="Nama Pengambil">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Jumlah Beli</label>
                    <div class="col-sm-8">
                      <input type="text" name="jumkeluar" class="form-control" id="jumkeluar" placeholder="Jumlah Keluar">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Harga Jual</label>
                    <div class="col-sm-8">
                      <input type="text" name="hargajual" class="form-control" id="hargajual" placeholder="Harga Jual">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-4 col-form-label">Keterangan</label>
                    <div class="col-sm-8">
                      <input type="text" name="keterangan" class="form-control" id="keterangan" placeholder="Keterangan">
                    </div>
                  </div>

                </div>
              </form>
              <div class="table-responsive">
                <table width="100%"  class="table table-striped table-sm">
                  <thead>
                    <tr>
                      <!-- <th>#</th> -->
                      <th>Kode Barang</th>
                      <th>Nama Barang </th>
                      <th>Serial</th>
                      <th>Jumlah</th>
                      <th>Harga</th>
                      <th>Harga Total</th>
                      <th>Ket.</th>
                      <!-- <th>Action</th> -->
                    </tr>
                  </thead>
                  <tbody id="tb_pembelian">

                  </tbody>
                  <tfoot id="foot">
                    <tr>
                      <td colspan="5" style="text-align: center;"><b> TOTAL</b></td>
                      <td> <input type="text" name="totalkeluar" id="totalkeluar" class="form-control col-sm-4" readonly="" onkeyup="sumbayar()"> </td>
                    </tr>
                    <tr>
                      <td colspan="5" style="text-align: center;"><b> Bayar </b></td>
                      <td> <input type="text" name="bayarkeluar" id="bayarkeluar" class="form-control col-sm-4" onkeyup="sumbayar()"> </td>
                    </tr>
                    <tr>
                      <td colspan="5" style="text-align: center;"><b> Sisa </b></td>
                      <td> <input type="text" name="sisakeluar" id="sisakeluar" class="form-control col-sm-4" onkeyup="sumbayar()"> </td>
                    </tr>
                  </tfoot>
                </table>

                <button class="btn btn-success float-right bt_simpan_entry" type="button"><i class="fa fa-save"></i> Simpan</button>
              </div>

            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_form_layanan01" role="dialog">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Detail</h3>
      </div>
      <div class="modal-body form">
        <div class="container-fluid">
          <div class="table-responsive">
            <table width="100%" id="detail_rpt01" class="table table-striped table-bordered table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Serial</th>
                  <th>Merek</th>
                  <th>Jml Keluar</th>
                  <th>Harga Jual</th>
                  <th>Ket. </th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_form_layanan02" role="dialog">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Detail</h3>
      </div>
      <div class="modal-body form">
        <div class="container-fluid">
          <div class="table-responsive">
            <table width="100%" id="detail_rpt02" class="table table-striped table-bordered table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>ID Transaksi</th>
                  <th>Angsuran</th>
                  <th>Tgl. Pembayaran</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>