<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <button type="button" id="userpelanggan" class="btn btn-primary" data-toggle="modal" data-target="#modal-userpelanggan" title="Tambah Data Area" > Tambah
            </button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table width="100%" id="tbuserpelanggan" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama User</th>
                    <th>Username</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modal-userpelanggan">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah User Pelanggan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Pelanggan</label>
              <div class="col-sm-10">
                <select class="form-control select2 pelanggan" name="pelangganuser" id="pelangganuser" style="width: 100%;">
                  <option >Pilih Pelanggan</option>
                  <?php foreach ($data as $key) : ?>
                    <option value="<?php echo $key->idpelanggan; ?>"><?php echo $key->namapelanggan; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">Username</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="usernameuser" name="usernameuser" placeholder="Username">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" id="passworduser" name="passworduser" placeholder="Password">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">Level</label>
              <div class="col-sm-10">
                <select class="form-control select2" id="leveluser" name="leveluser" readonly="true">
                  <option value="3"> Pelanggan </option>
                </select>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary simpan_user_pelanggan">Simpan</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-edit-penggunauser">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Pengguna</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal">
          <div class="card-body">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">ID</label>
              <div class="col-sm-10">
                <input type="text" type="hidden" name="idpenggunauser" class="form-control" id="idpenggunauser" placeholder="ID" readonly="true">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">Nama User</label>
              <div class="col-sm-10">
                <input type="text" name="editnamapenggunauser" class="form-control" id="editnamapenggunauser" placeholder="Nama User" readonly="">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">Username</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="editusernameuser" name="editusernameuser" placeholder="Harga">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" id="editpassworduser" name="editpassworduser" placeholder="Password">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary simpan_penggunauser_edit">Simpan</button>
      </div>
    </div>
  </div>
</div>