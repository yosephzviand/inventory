<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<button type="button" id="pdfdesa" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-rak" title="Tambah Data Rak"> Tambah
						</button>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table width="100%" id="rak" class="table table-striped table-bordered table-sm">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="modal-rak">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Rak</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="card-body">
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-2 col-form-label">Nama Rak</label>
							<div class="col-sm-10">
								<input type="text" name="namarak" class="form-control" id="namarak" placeholder="Nama Rak">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary simpan_rak">Simpan</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modal-edit-rak">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Rak</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="card-body">
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-2 col-form-label">ID</label>
							<div class="col-sm-10">
								<input type="text" name="idrak" class="form-control" id="idrak" placeholder="ID" readonly="true">
							</div>
						</div>
						<div class="form-group row">
							<label for="inputEmail3" class="col-sm-2 col-form-label">Nama Area</label>
							<div class="col-sm-10">
								<input type="text" name="editnamarak" class="form-control" id="editnamarak" placeholder="Nama Rak">
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary simpan_rak_edit">Simpan</button>
			</div>
		</div>
	</div>
</div>
