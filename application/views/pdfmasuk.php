<table border="">
	<tr>
		<td rowspan="" style="text-align: center;" width="200px"><img src="<?php echo base_url() ?>assets/dist/img/gesitlogokecil.PNG" width="120px" alt="AdminLTE Logo"></td>
		<td>
			<td>
				<p style="font-size: 18px"><b>GEKA SOLUSI UTAMA</b></p>
				<p style="font-size: 12px">Jalan Yogya-Wonosari Km. 17,5 Patuk Gunungkidul Yogyakarta 55862</p>
				<p style="font-size: 12px">Telp  : 085225225959 (CS) Email : atomedia_mail@yahoo.com</p>
			</td>
		</tr>
	</table>
	<hr>

	<h3 style="text-align: center;">Data Barang Masuk </h3>
	<table width="100%" cellspacing="0" cellpadding="3" style="font-size: 9pt;" border="1">
		<thead>
			<tr>
				<th>No</th>
				<th>Kode Barang</th>
				<th>Nama Barang</th>
				<th>Serial</th>
				<th>Tgl. Masuk</th>
				<th>Supplier</th>
				<th>Jumlah</th>
				<th>Harga Satuan</th>
				<th>Total Harga</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1; foreach ($data as $key) :?>
			<tr>
				<td style="text-align: center;"><?= $no++ ?></td>
				<td><?= $key->kodebarang ?></td>
				<td><?= $key->namabarang ?></td>
				<td><?= $key->serialnumber ?></td>
				<td><?= $key->tglmasuk ?></td>
				<td><?= $key->namasup ?></td>
				<td style="text-align: right;"><?= $key->stokawal ?></td>
				<td style="text-align: right;"><?= number_format($key->hargabeli,0,',','.') ?></td>
				<td style="text-align: right;"><?= number_format($key->totalharga,0,',','.') ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<?php foreach ($sum as $key) : ?>
			<tr>
				<td colspan="6" style="text-align: center;"><b>TOTAL</b></td>
				<td style="text-align: right;"><?= $key->jumlah ?></td>
				<td style="text-align: right;"><?= number_format($key->sumharga,0,',','.') ?></td>
				<td style="text-align: right;"><?= number_format($key->sumtotal,0,',','.') ?></td>
			</tr>
		<?php endforeach ?>
	</tfoot>
</table>