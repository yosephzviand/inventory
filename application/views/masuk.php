<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<button type="button" id="pdfdesa" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-masuk" title="Tambah Data Area"> Tambah
						</button>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table width="100%" id="tbmasuk" class="table table-striped table-bordered table-sm">
								<thead>
									<tr>
										<th>No</th>
										<th>Kode Barang</th>
										<th>Nama Barang </th>
										<th>Serial</th>
										<th>Jumlah</th>
										<th>Tgl. Masuk</th>
										<th>Supplier</th>
										<th>Batas Garansi</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="modal-masuk">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Barang</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form" id="formMasuk">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 col-12">
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Kategori</label>
									<div class="col-sm-10">
										<select class="form-control select2" id="masukkategori" name="masukkategori">
											<option value="">Pilih Kategori</option>
											<?php foreach ($kategori as $key) : ?>
												<option value="<?php echo $key->idkategori; ?>"><?php echo $key->namakategori; ?></option>
											<?php endforeach; ?>

										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-2 col-form-label">Kode </label>
									<div class="col-sm-10">
										<input type="text" name="kodebarang" class="form-control" id="kodebarang" placeholder="Kode Barang">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-2 col-form-label">Nama </label>
									<div class="col-sm-10">
										<input type="text" name="namabarang" class="form-control" id="namabarang" placeholder="Nama Barang">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Serial</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="serial" name="serial" placeholder="Serial Number">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Merek</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="merek" name="merek" placeholder="Merek">
									</div>
								</div>
							</div>

							<div class="col-lg-6 col-12">
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Harga Beli</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="hargabeli" name="hargabeli" placeholder="Harga Beli">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Garansi</label>
									<div class="col-sm-10">
										<input name="garansi" type="text" class="form-control pull-right" id="garansi" placeholder="Akhir Garansi" required="true">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Tgl. Masuk</label>
									<div class="col-sm-10">
										<input name="datepicker" type="text" class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" required="true">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Jumlah</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah Barang">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Supplier</label>
									<div class="col-sm-10">
										<select class="form-control select2" id="masuksupplier" name="masuksupplier">
											<option value="">Pilih Supplier</option>
											<?php foreach ($supplier as $key) : ?>
												<option value="<?php echo $key->idsup; ?>"><?php echo $key->namasup; ?></option>
											<?php endforeach; ?>

										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Ekspedisi</label>
									<div class="col-sm-10">
										<select class="form-control select2" id="masukekspedisi" name="masukekspedisi">
											<option value="">Pilih Ekspedisi</option>
											<?php foreach ($ekspedisi as $key) : ?>
												<option value="<?php echo $key->id; ?>"><?php echo $key->tujuan . ' / ' . $key->tanggal; ?></option>
											<?php endforeach; ?>

										</select>
									</div>
								</div>

							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-primary simpan_masuk" id="">Simpan</button>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="modal-edit-masuk">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Barang</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 col-12">
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-2 col-form-label">ID</label>
									<div class="col-sm-10">
										<input type="text" type="hidden" name="idbarang" class="form-control" id="idbarang" placeholder="ID" readonly="true">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Kategori</label>
									<div class="col-sm-10">
										<select class="form-control select2" id="editmasukkategori" name="editmasukkategori">
											<option value="">Pilih Kategori</option>
											<?php foreach ($kategori as $key) : ?>
												<option value="<?php echo $key->idkategori; ?>"><?php echo $key->namakategori; ?></option>
											<?php endforeach; ?>

										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-2 col-form-label">Kode </label>
									<div class="col-sm-10">
										<input type="text" name="editkodebarang" class="form-control" id="editkodebarang" placeholder="Kode Barang">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-2 col-form-label">Nama </label>
									<div class="col-sm-10">
										<input type="text" name="editnamabarang" class="form-control" id="editnamabarang" placeholder="Nama Barang">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Serial</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="editserial" name="editserial" placeholder="Serial Number">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Merek</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="editmerek" name="editmerek" placeholder="Merek">
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-12">
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Harga Beli</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="edithargabeli" name="edithargabeli" placeholder="Harga Beli">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Garansi</label>
									<div class="col-sm-10">
										<input name="editgaransi" type="text" class="form-control pull-right" id="editgaransi" placeholder="Akhir Garansi" required="true">
									</div>
								</div>

								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Tgl. Masuk</label>
									<div class="col-sm-10">
										<input name="datepicker" type="text" class="form-control pull-right" id="datepicker1" placeholder="Masukkan Tanggal" required="true">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Jumlah</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="editjumlah" name="editjumlah" placeholder="Jumlah Barang">
									</div>
								</div>
								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Supplier</label>
									<div class="col-sm-10">
										<select class="form-control select2" id="editmasuksupplier" name="editmasuksupplier">
											<option value="">Pilih Supplier</option>
											<?php foreach ($supplier as $key) : ?>
												<option value="<?php echo $key->idsup; ?>"><?php echo $key->namasup; ?></option>
											<?php endforeach; ?>

										</select>
									</div>
								</div>

								<div class="form-group row">
									<label for="inputPassword3" class="col-sm-2 col-form-label">Ekspedisi</label>
									<div class="col-sm-10">
										<select class="form-control select2" id="editmasukekspedisi" name="editmasukekspedisi">
											<option value="">Pilih Ekspedisi</option>
											<?php foreach ($ekspedisi as $key) : ?>
												<option value="<?php echo $key->id; ?>"><?php echo $key->tujuan . ' / ' . $key->tanggal; ?></option>
											<?php endforeach; ?>

										</select>
									</div>
								</div>

							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer justify-content-between">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-primary simpan_masuk_edit">Simpan</button>
			</div>
		</div>
	</div>
</div>
