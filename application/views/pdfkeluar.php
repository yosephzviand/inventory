<table border="">
	<tr>
		<td rowspan="" style="text-align: center;" width="200px"><img src="<?php echo base_url() ?>assets/dist/img/gesitlogokecil.PNG" width="120px" alt="AdminLTE Logo"></td>
		<td>
			<td>
				<p style="font-size: 18px"><b>GEKA SOLUSI UTAMA</b></p>
				<p style="font-size: 12px">Jalan Yogya-Wonosari Km. 17,5 Patuk Gunungkidul Yogyakarta 55862</p>
				<p style="font-size: 12px">Telp  : 085225225959 (CS) Email : atomedia_mail@yahoo.com</p>
			</td>
		</tr>
	</table>
	<hr>

	<h3 style="text-align: center;">Data Barang Keluar </h3>
	<table width="100%" cellspacing="0" cellpadding="3" style="font-size: 9pt;" border="1">
		<thead>
			<tr>
				<th>No</th>
				<th>Kode Barang</th>
				<th>Nama Barang</th>
				<th>Serial</th>
				<th>Tgl. Keluar</th>
				<th>Pengambil</th>
				<th>Harga</th>
				<th>Jumlah</th>
				<th>Ket.</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$no = 1; foreach ($data as $key) : ?>
			?>
			<tr>
				<td style="text-align: center;"><?= $no++ ?></td>
				<td><?= $key->kodebarang ?></td>
				<td><?= $key->namabarang ?></td>
				<td><?= $key->serialnumber ?></td>
				<td><?= $key->tglkeluar ?></td>
				<td><?= $key->namapembeli ?></td>
				<td style="text-align: right;"><?= number_format($key->hargajual,0,',','.') ?></td>
				<td style="text-align: right;"><?= $key->jmlkeluar ?></td>
				<td><?= $key->keterangan ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<?php foreach ($sum as $key) : ?>
			<tr>
				<td colspan="6" style="text-align: center;"><b>TOTAL</b></td>
				<td style="text-align: right;"> <b><?= number_format($key->sumharga,0,',','.') ?></b></td>
				<td style="text-align: right;"> <b><?= number_format($key->jumlah,0,',','.') ?></b></td>
			</tr>
		<?php endforeach ?>
	</tfoot>
</table>