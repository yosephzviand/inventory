<table border="">
	<tr>
		<td rowspan="" style="text-align: center;" width="200px"><img src="<?php echo base_url() ?>assets/dist/img/gesitlogokecil.PNG" width="120px" alt="AdminLTE Logo"></td>
		<td>
		<td>
			<p style="font-size: 18px"><b>GEKA SOLUSI UTAMA</b></p>
			<p style="font-size: 12px">Jalan Yogya-Wonosari Km. 17,5 Patuk Gunungkidul Yogyakarta 55862</p>
			<p style="font-size: 12px">Telp : 085225225959 (CS) Email : atomedia_mail@yahoo.com</p>
		</td>
	</tr>
</table>
<hr>

<h3 style="text-align: center;">Data Rekap Laba dan Barang Terjual </h3>
<table width="100%" cellspacing="0" cellpadding="3" style="font-size: 9pt;" border="1">
	<thead>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Nama Barang</th>
			<th colspan="2">Januari</th>
			<th colspan="2">Februari</th>
			<th colspan="2">Maret</th>
			<th colspan="2">April</th>
			<th colspan="2">Mei</th>
			<th colspan="2">Juni</th>
			<th colspan="2">Juli</th>
			<th colspan="2">Agustus</th>
			<th colspan="2">September</th>
			<th colspan="2">Oktober</th>
			<th colspan="2">November</th>
			<th colspan="2">Desember</th>
		</tr>
		<tr>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
			<th>Terjual</th>
			<th>Laba</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$no = 1;
		foreach ($data as $key) :
		?>
			<tr>
				<td style="text-align: center;"><?= $no++ ?></td>
				<td><?= $key->namabarang ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarjanuari, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungjanuari, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarfebruari, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungfebruari, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarmaret, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungmaret, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarapril, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungapril, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarmei, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungmei, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarjuni, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungjuni, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarjuli, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungjuli, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluaragustus, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungagustus, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarseptember, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungseptember, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluaroktober, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungoktober, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarnovember, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungnovember, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluardesember, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->untungdesember, 0, ',', '.') ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<?php
		$no = 1;
		foreach ($sum as $key) :
		?>
			<tr>
				<td colspan="2" style="text-align: center;"><b>TOTAL</b></td>
				<td style="text-align: right;"><?= number_format($key->keluarjanuari, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->Januari, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarfebruari, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->Februari, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarmaret, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->Maret, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarapril, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->April, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarmei, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->Mei, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarjuni, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->Juni, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarjuli, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->Juli, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluaragustus, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->Agustus, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarseptember, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->September, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluaroktober, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->Oktober, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluarnovember, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->November, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->keluardesember, 0, ',', '.') ?></td>
				<td style="text-align: right;"><?= number_format($key->Desember, 0, ',', '.') ?></td>
			</tr>
		<?php endforeach; ?>
	</tfoot>
</table>
