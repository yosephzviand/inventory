<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card card-primary card-outline">
					<div class="card-header">
						<h4 class="card-title">
							Barang Keluar
						</h4>
					</div>
					<div class="card-body">
						<div class="row">								
							<div class="col-lg-6 col-12">
								<form  id="pembelian" method="POST">
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Tanggal Penjualan</label>
										<div class="col-sm-6">
											<input type="text" name="tglpenjualan" class="form-control" id="datepicker" placeholder="Tanggal Keluar" >
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Barang</label>
										<div class="col-sm-6">
											<select class="form-control select2" name="barangkeluar" id="barangkeluar" style="width: 100%;">
												<option value="" >Pilih Barang</option>
												<?php foreach ($barang as $key) : ?>
													<option value="<?php echo $key->idbarang; ?>"><?php echo $key->namabarang; ?> (<?= $key->serialnumber ?>)</option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>

									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Stok</label>
										<div class="col-sm-6">
											<input type="text" name="stok" class="form-control" id="stok" placeholder="Stok" readonly="">
										</div>
									</div>

									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label"></label>
										<div class="col-sm-5">
											<button type="button" class="btn btn-primary simpan_keluar">Tambah Barang</button>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-12">
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Nama Pengambil</label>
										<div class="col-sm-6">
											<input type="text" name="namapengambil" class="form-control" id="namapengambil" placeholder="Nama Pengambil">
										</div>
									</div>

									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Jumlah Beli</label>
										<div class="col-sm-6">
											<input type="text" name="jumkeluar" class="form-control" id="jumkeluar" placeholder="Jumlah Keluar">
										</div>
									</div>
									
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Harga Jual</label>
										<div class="col-sm-6">
											<input type="text" name="hargajual" class="form-control" id="hargajual" placeholder="Harga Jual">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-3 col-form-label">Keterangan</label>
										<div class="col-sm-6">
											<input type="text" name="keterangan" class="form-control" id="keterangan" placeholder="Keterangan">
										</div>
									</div>

								</div>
							</form>
							<div class="table-responsive">
								<table width="100%"  class="table table-striped table-sm">
									<thead>
										<tr>
											<!-- <th>#</th> -->
											<th>Kode Barang</th>
											<th>Nama Barang </th>
											<th>Serial</th>
											<th>Jumlah</th>
											<th>Harga</th>
											<th>Harga Total</th>
											<th>Ket.</th>
											<!-- <th>Action</th> -->
										</tr>
									</thead>
									<tbody id="tb_pembelian">

									</tbody>
									<tfoot id="foot">
										<tr>
											<td colspan="5" style="text-align: center;"><b> TOTAL</b></td>
											<td> <input type="text" name="total" id="total" class="form-control col-sm-4" readonly="" onkeyup="sumbayar()"> </td>
										</tr>
										<tr>
											<td colspan="5" style="text-align: center;"><b> Bayar </b></td>
											<td> <input type="text" name="bayar" id="bayar" class="form-control col-sm-4" onkeyup="sumbayar()"> </td>
										</tr>
										<tr>
											<td colspan="5" style="text-align: center;"><b> Sisa </b></td>
											<td> <input type="text" name="sisa" id="sisa" class="form-control col-sm-4" onkeyup="sumbayar()"> </td>
										</tr>
									</tfoot>
								</table>

								<button class="btn btn-success float-right bt_simpan_entry" type="button"><i class="fa fa-save"></i> Simpan</button>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>