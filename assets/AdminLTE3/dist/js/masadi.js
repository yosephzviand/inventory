jQuery(document).ready(function($) {
    // $('.select2').select2();
    $(".bt_simpan_entry").attr('disabled', 'disabled');
    $(".bt_simpan_ekspedisi").attr('disabled', 'disabled');

    $('#datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });

    $('#datepicker1').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });

    $('#datepicker2').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });

    $('#datepicker3').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });

    $('#garansi').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });

    $('#editgaransi').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
    });

    kategori = $('#kategori').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxkategori",
            "type": "POST"
        },

    });
    rak = $('#rak').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxrak",
            "type": "POST"
        },

    });

    supplier = $('#supplier').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxsuplier",
            "type": "POST"
        },

    });

    masuk = $('#tbmasuk').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxmasuk",
            "type": "POST"
        },

    });

    pengguna = $('#tbpengguna').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxpengguna",
            "type": "POST"
        },

    });

    keluar = $('#tbkeluar').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxkeluar",
            "type": "POST"
        },

    });

    tbpembayaran = $('#tbpembayaran').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxpembayaran",
            "type": "POST"
        },

    });

    tbdinas = $('#tbdinas').DataTable({

        "ajax": {
            "url": site_url + "amc/ajaxdinas",
            "type": "POST"
        },

    });


    // Kategori
    $('.content-wrapper').on('click', '.editkategori', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editkategori",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idkategori"]').val(data[i].idkategori);
                    $('[name="editnamakategori"]').val(data[i].namakategori);
                    $('#modal-edit-kategori').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_kategori', function(e) {
        e.preventDefault();
        var namakategori = $('#namakategori').val();
        if (namakategori == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertkategori",
                data: { namakategori: namakategori },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-kategori').modal('hide');
                    $('#namakategori').val('');
                    kategori.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_kategori_edit', function(e) {
        e.preventDefault();
        var idkategori = $('#idkategori').val();
        var namakategori = $('#editnamakategori').val();
        if (idkategori == '' || namakategori == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatekategori",
                data: { idkategori: idkategori, namakategori: namakategori },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-kategori').modal('hide');
                    $('#idkategori').val('');
                    $('#editnamakategori').val('');
                    kategori.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapuskategori', function(e) {
        e.preventDefault();
        var idkategori = $(this).data('kode');
        swal({
            title: 'Delete Data',
            text: 'Yakin Ingin Menghapus Data ?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        }, function() {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/deletekategori",
                data: { idkategori: idkategori },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    kategori.ajax.reload(null, false);
                }
            })
        })
    });

    // Supplier

    $('.content-wrapper').on('click', '.editsupplier', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editsupplier",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idsupplier"]').val(data[i].idsup);
                    $('[name="editnamasupplier"]').val(data[i].namasup);
                    $('[name="editalamat"]').val(data[i].alamatsup);
                    $('[name="editnotelp"]').val(data[i].notelpsup);
                    $('#modal-edit-supplier').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_supplier', function(e) {
        e.preventDefault();
        var namasupplier = $('#namasupplier').val();
        var alamat = $('#alamat').val();
        var notelp = $('#notelp').val();
        if (namasupplier == '' || alamat == '' || notelp == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertsupplier",
                data: { namasupplier: namasupplier, alamat: alamat, notelp: notelp },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-paket').modal('hide');
                    $('#namasupplier').val('');
                    $('#alamat').val('');
                    $('#notelp').val('');
                    supplier.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_supplier_edit', function(e) {
        e.preventDefault();
        var idsupplier = $('#idsupplier').val();
        var namasupplier = $('#editnamasupplier').val();
        var alamat = $('#editalamat').val();
        var notelp = $('#editnotelp').val();
        if (idsupplier == '' || namasupplier == '' || alamat == '' || notelp == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatesupplier",
                data: { idsupplier: idsupplier, namasupplier: namasupplier, alamat: alamat, notelp: notelp },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-supplier').modal('hide');
                    $('#idsupplier').val('');
                    $('#editnamasupplier').val('');
                    $('#editalamat').val('');
                    $('#editnotelp').val('');
                    supplier.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapussupplier', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        swal({
            title: 'Delete Data',
            text: 'Yakin Ingin Menghapus Data ?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        }, function() {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/deletesupplier",
                data: { id: id },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    supplier.ajax.reload(null, false);
                }
            })
        })
    });

    //

    $('.content-wrapper').on('click', '.editmasuk', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editmasuk",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idbarang"]').val(data[i].idbarang);
                    $('[name="editmasukkategori"]').val(data[i].idkategori);
                    $('[name="editkodebarang"]').val(data[i].kodebarang);
                    $('[name="editnamabarang"]').val(data[i].namabarang);
                    $('[name="editserial"]').val(data[i].serialnumber);
                    $('[name="editmerek"]').val(data[i].merek);
                    $('[name="datepicker"]').val(data[i].tglmasuk);
                    $('[name="edithargabeli"]').val(data[i].hargabeli);
                    $('[name="editgaransi"]').val(data[i].garansi);
                    $('[name="editjumlah"]').val(data[i].stok);
                    $('[name="editmasuksupplier"]').val(data[i].idsupp);
                    $('#modal-edit-masuk').modal('show');
                }
            }
        })
    });



    $('.content-wrapper').on('click', '.simpan_masuk', function(e) {
        e.preventDefault();
        var kodebarang = $('#kodebarang').val();
        var namabarang = $('#namabarang').val();
        var jumlah = $('#jumlah').val();
        var serial = $('#serial').val();
        var tglmasuk = $('#datepicker').val();
        var supplier = $('#masuksupplier').val();
        var ekspedisi = $('#masukekspedisi').val();
        var merek = $('#merek').val();
        var kategori = $('#masukkategori').val();
        var hargabeli = $('#hargabeli').val();
        var garansi = $('#garansi').val();
        if (kodebarang == '' || namabarang == '' || jumlah == '' || serial == '' || tglmasuk == '' || supplier == '' || merek == '' || kategori == '' || hargabeli == '' || ekspedisi == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertmasuk",
                data: { ekspedisi: ekspedisi, kodebarang: kodebarang, namabarang: namabarang, jumlah: jumlah, serial: serial, tglmasuk: tglmasuk, supplier: supplier, merek: merek, kategori: kategori, hargabeli: hargabeli, garansi: garansi },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-masuk').modal('hide');
                    $('#kodebarang').val('');
                    $('#namabarang').val('');
                    $('#jumlah').val('');
                    $('#serial').val('');
                    $('#datepicker').val('');
                    $('#masuksupplier').val('');
                    $('#masukekspedisi').val('');
                    $('#merek').val('');
                    $('#masukkategori').val('');
                    $('#hargabeli').val('');
                    $('#garansi').val('');
                    masuk.ajax.reload(null, false);
                }
            })
        }
    });


    $('.content-wrapper').on('click', '.simpan_masuk_edit', function(e) {
        e.preventDefault();
        var idbarang = $('#idbarang').val();
        var kodebarang = $('#editkodebarang').val();
        var namabarang = $('#editnamabarang').val();
        var jumlah = $('#editjumlah').val();
        var serial = $('#editserial').val();
        var tglmasuk = $('#datepicker1').val();
        var supplier = $('#editmasuksupplier').val();
        var merek = $('#editmerek').val();
        var kategori = $('#editmasukkategori').val();
        var hargabeli = $('#edithargabeli').val();
        var garansi = $('#editgaransi').val();
        var ekspedisi = $('#editmasukekspedisi').val();

        if (idbarang == '' || kodebarang == '' || namabarang == '' || jumlah == '' || serial == '' || tglmasuk == '' || supplier == '' || merek == '' || kategori == '' || hargabeli == '' || ekspedisi == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatemasuk",
                data: { ekspedisi: ekspedisi, idbarang: idbarang, kodebarang: kodebarang, namabarang: namabarang, jumlah: jumlah, serial: serial, tglmasuk: tglmasuk, supplier: supplier, merek: merek, kategori: kategori, hargabeli: hargabeli, garansi: garansi },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-masuk').modal('hide');
                    $('#idbarang').val('');
                    $('#editkodebarang').val('');
                    $('#editnamabarang').val('');
                    $('#editjumlah').val('');
                    $('#editserial').val('');
                    $('#datepicker1').val('');
                    $('#editmasuksupplier').val('');
                    $('#editmerek').val('');
                    $('#editmasukkategori').val('');
                    $('#edithargabeli').val('');
                    $('#editgaransi').val('');
                    $('#editmasukekspedisi').val('');
                    masuk.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapusmasuk', function(e) {
        e.preventDefault();
        var idbarang = $(this).data('kode');
        var id = $(this).data('kode');
        swal({
            title: 'Delete Data',
            text: 'Yakin Ingin Menghapus Data ?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        }, function() {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/deletemasuk",
                data: { idbarang: idbarang },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    masuk.ajax.reload(null, false);
                }
            })
        })
    });

    $('.content-wrapper').on('click', '.editpengguna', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editpengguna",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idpengguna"]').val(data[i].idusers);
                    $('[name="editnamapengguna"]').val(data[i].nama);
                    $('[name="editusername"]').val(data[i].username);
                    $('#modal-edit-pengguna').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_pengguna', function(e) {
        e.preventDefault();
        var namapengguna = $('#namapengguna').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var level = $('#level').val();
        if (namapengguna == '' || username == '' || password == '' || level == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertpengguna",
                data: { namapengguna: namapengguna, username: username, password: password, level: level },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-pengguna').modal('hide');
                    $('#namapengguna').val('');
                    $('#username').val('');
                    $('#password').val('');
                    $('#level').val('');
                    pengguna.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_pengguna_edit', function(e) {
        e.preventDefault();
        var idpengguna = $('#idpengguna').val();
        var editnamapengguna = $('#editnamapengguna').val();
        var editusername = $('#editusername').val();
        var editpassword = $('#editpassword').val();
        var editlevel = $('#editlevel').val();
        if (idpengguna == '' || editnamapengguna == '' || editusername == '' || editpassword == '' || editlevel == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatepengguna",
                data: { idpengguna: idpengguna, editnamapengguna: editnamapengguna, editusername: editusername, editpassword: editpassword, editlevel: editlevel },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-pengguna').modal('hide');
                    $('#idpengguna').val('');
                    $('#editnamapengguna').val('');
                    $('#editusername').val('');
                    $('#editpassword').val('');
                    $('#editlevel').val('');
                    pengguna.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapuspengguna', function(e) {
        e.preventDefault();
        var idpengguna = $(this).data('kode');
        swal({
            title: 'Delete Data',
            text: 'Yakin Ingin Menghapus Data ?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        }, function() {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/deletepengguna",
                data: { idpengguna: idpengguna },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    pengguna.ajax.reload(null, false);
                }
            })
        })
    });

    //

    $('.content-wrapper').on('click', '.edittagihan', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_edittagihan",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idtagihan"]').val(data[i].idtransaksi);
                    $('[name="editbulan"]').val(data[i].bulan);
                    $('[name="edittambahan"]').val(data[i].penambahan);
                    $('[name="editnamapelanggan"]').val(data[i].namapelanggan);
                    $('[name="editpakettagihan"]').val(data[i].namapaket);
                    $('[name="edittagihan"]').val(data[i].harga);
                    $('#modal-edit-tagihan').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.tambah_keluar', function(e) {
        e.preventDefault();
        var idkategori = $('#idkategori').val();
        var sisa = $('#stok').val();
        var qty = $('#jumkeluar').val();

        if (qty > sisa) {
            swal({
                title: "Error!",
                text: "Jumlah beli melebihi stok",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        }
        if (qty == '') {
            swal({
                title: "Error!",
                text: "Jumlah beli harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        }
        $.ajax({
            type: 'POST',
            url: site_url + "amc/insertkeluar",
            data: { idkategori: idkategori, idbarang: idbarang, tglkeluar: tglkeluar, jmlkeluar: jmlkeluar, nama: nama },
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                toastr[data.alert](data.data, data.title);
                $('#modal-keluar').modal('hide');
                $('#idkategori').val('');
                $('#stok').val('');
                $('#jumkeluar').val('');
                keluar.ajax.reload(null, false);
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_tagihan_edit', function(e) {
        e.preventDefault();
        var idtagihan = $('#idtagihan').val();
        var bulan = $('#editbulan').val();
        var tagihan = $('#edittotaltagihan').val();
        var tambahan = $('#edittambahan').val();
        $.ajax({
            type: 'POST',
            url: site_url + "amc/updatetagihan",
            data: { idtagihan: idtagihan, bulan: bulan, tagihan: tagihan, tambahan: tambahan },
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                toastr[data.alert](data.data, data.title);
                $('#modal-edit-tagihan').modal('hide');
                $('#idtagihan').val('');
                $('#editbulan').val('');
                $('#edittotaltagihan').val('');
                $('#edittambahan').val('');
                tbtagihan.ajax.reload(null, false);
            }
        })
    });

    $('.content-wrapper').on('click', '.hapustagihan', function(e) {
        e.preventDefault();
        var idtransaksi = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/deletetagihan",
            data: { idtransaksi: idtransaksi },
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                toastr[data.alert](data.data, data.title);
                // $('#modal-edit-pelanggan').modal('hide');
                tbtagihan.ajax.reload(null, false);
            }
        })
    });

    $('.content-wrapper').on('click', '.pdftagihan', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        window.open(site_url + "amc/pdf/" + id, '_blank');
    });


    $('.content-wrapper').on('click', '.bayar', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_pembayaran",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idtransaksi"]').val(data[i].idtransaksi);
                    $('[name="namapembeli"]').val(data[i].namapembeli);
                    $('[name="total"]').val(data[i].total);
                    $('[name="kurang"]').val(data[i].sisa);
                    $('#modal-pembayaran').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_pembayaran', function(e) {
        e.preventDefault();
        var id = $('#idtransaksi').val();
        var bayar = $('#bayar').val();
        var sisa = $('#sisa').val();
        // var level =  $('#level').val();
        if (bayar == '' || sisa == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatepembayaran",
                data: { id: id, bayar: bayar, sisa: sisa },
                dataType: "JSON",
                success: function(data, status) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-pembayaran').modal('hide');
                    keluar.ajax.reload(null, false);

                    if (status == 'success') {
                        $('#idtransaksi').val('');
                        $('#bayar').val('');
                        $('#sisa').val('');
                        window.open(site_url + "amc/pdfangsuran/" + id, '_blank');
                    }
                }
            })
        }


    });

    $('.content-wrapper').on('click', '.hapuspembayaran', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editpembayaran",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idtransaksi"]').val(data[i].idtransaksi);
                    $('#modal-hapus-pembayaran').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_hapus_pembayaran', function(e) {
        e.preventDefault();
        var id = $('#idtransaksi').val();
        console.log(id);
        $.ajax({
            type: 'POST',
            url: site_url + "amc/updatehapuspembayaran",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                toastr[data.alert](data.data, data.title);
                $('#modal-hapus-pembayaran').modal('hide');
                tbpembayaran.ajax.reload(null, false);
            }
        })
    });

    $('.content-wrapper').on('click', '.pdfpembayaran', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        window.open(site_url + "amc/pdfpembayaran/" + id, '_blank');
    });


    $('.content-wrapper').on('click', '.edituserpelanggan', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editpengguna",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="idpenggunauser"]').val(data[i].idusers);
                    $('[name="editnamapenggunauser"]').val(data[i].namapelanggan);
                    $('[name="editusernameuser"]').val(data[i].username);
                    $('#modal-edit-penggunauser').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_user_pelanggan', function(e) {
        e.preventDefault();
        var pelangganuser = $('#pelangganuser').val();
        var usernameuser = $('#usernameuser').val();
        var passworduser = $('#passworduser').val();
        var leveluser = $('#leveluser').val();
        if (pelangganuser == '' || usernameuser == '' || passworduser == '' || leveluser == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertuserpelanggan",
                data: { pelangganuser: pelangganuser, usernameuser: usernameuser, passworduser: passworduser, leveluser: leveluser },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-userpelanggan').modal('hide');
                    $('#pelangganuser').val('');
                    $('#usernameuser').val('');
                    $('#passworduser').val('');
                    $('#leveluser').val('');
                    tbuserpelanggan.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_penggunauser_edit', function(e) {
        e.preventDefault();
        var idpenggunauser = $('#idpenggunauser').val();
        var editusernameuser = $('#editusernameuser').val();
        var editpassworduser = $('#editpassworduser').val();
        if (idpenggunauser == '' || editusernameuser == '' || editpassworduser == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updateuserpelanggan",
                data: { idpenggunauser: idpenggunauser, editusernameuser: editusernameuser, editpassworduser: editpassworduser },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-penggunauser').modal('hide');
                    $('#idpenggunauser').val('');
                    $('#editusernameuser').val('');
                    $('#editpassworduser').val('');
                    tbuserpelanggan.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapususerpelanggan', function(e) {
        e.preventDefault();
        var idpenggunauser = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/deleteuserpelanggan",
            data: { idpenggunauser: idpenggunauser },
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                toastr[data.alert](data.data, data.title);
                // $('#modal-edit-pelanggan').modal('hide');
                tbuserpelanggan.ajax.reload(null, false);
            }
        })
    });

    $.ajax({
        url: site_url + "amc/chart",
        method: "GET",
        dataType: "JSON",
        success: function(data) {
            var label = [];
            var stok = [];
            var keluar = [];
            var d = new Date();
            var n = d.getFullYear();
            for (var i in data) {
                label.push(data[i].namabarang);
                stok.push(Number(data[i].stok));
                keluar.push(Number(data[i].jumlah));
            }

            Highcharts.chart('container', {

                chart: {
                    type: 'column'
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },

                title: {
                    text: 'Data Barang'
                },
                xAxis: {
                    categories: label,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Pcs'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y} Pcs</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },

                series: [{
                    name: 'Stok',
                    data: stok,
                    color: '#0066ff'
                }, {
                    name: 'Keluar',
                    data: keluar,
                    color: '#ff6600'
                }]

            });

        }
    });

    $.ajax({
        url: site_url + "amc/chartkategori",
        method: "GET",
        dataType: "JSON",
        success: function(data) {
            var label = [];
            var stok = [];
            var keluar = [];
            var d = new Date();
            var n = d.getFullYear();
            var asu = [];
            var series = [];
            for (var i in data) {
                label.push(data[i].namakategori);
                stok.push(Number(data[i].jumlah));
            }

            for (var k = 0; k < label.length; k++) {
                series.push({
                    name: data[k].namakategori,
                    y: Number(stok[k])
                });
            }

            Highcharts.setOptions({
                colors: [
                    '#4572A7',
                    '#AA4643',
                    '#89A54E',
                    '#80699B',
                    '#3D96AE',
                    '#DB843D',
                    '#92A8CD',
                    '#A47D7C',
                    '#B5CA92',
                    '#90ed7d',
                    '#434348',
                    '#f7a35c',
                    '#8085e9',
                    '#f15c80',
                    '#e4d354',
                    '#2b908f',
                    '#f45b5b',
                    '#91e8e1',
                    '#7cb5ec',
                    '#434348',
                    '#90ed7d',
                    '#f7a35c',
                    '#8085e9',
                    '#f15c80',
                    '#e4d354',
                    '#2b908f',
                    '#f45b5b',
                    '#91e8e1'
                ]
            });

            Highcharts.chart('container12', {

                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },

                title: {
                    text: 'Data Kategori Barang'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                credits: {
                    enabled: false
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series:

                    [{
                    name: 'Jumlah',
                    colorByPoint: true,

                    data: series
                }]
            });

        }
    });


    $.ajax({
        url: site_url + "amc/chartuntung",
        method: "GET",
        dataType: "JSON",
        success: function(data) {
            var januari = [];
            var februari = [];
            var maret = [];
            var april = [];
            var mei = [];
            var juni = [];
            var juli = [];
            var agustus = [];
            var september = [];
            var oktober = [];
            var november = [];
            var desember = [];
            var d = new Date();
            var n = d.getFullYear();
            for (var i in data) {
                januari.push(Number(data[i].Januari));
                februari.push(Number(data[i].Februari));
                maret.push(Number(data[i].Maret));
                april.push(Number(data[i].April));
                mei.push(Number(data[i].Mei));
                juni.push(Number(data[i].Juni));
                juli.push(Number(data[i].Juli));
                agustus.push(Number(data[i].Agustus));
                september.push(Number(data[i].September));
                oktober.push(Number(data[i].Oktober));
                november.push(Number(data[i].November));
                desember.push(Number(data[i].Desember));
            }
            var cok = [januari, februari, maret, april, mei, juni, juli, agustus, september, oktober, november, desember];


            Highcharts.chart('container4', {
                chart: {
                    type: 'area'
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },

                title: {
                    text: 'Laba'
                },
                xAxis: {
                    categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli',
                        'Agustus', 'September', 'Oktober', 'November', 'Desember'
                    ]
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
                        ' <td style="padding:0"> <b>  Rp. {point.y} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    },
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },
                series: [{
                    name: 'Untung',
                    data: cok,
                    color: '#7FFF00'
                }]
            });

        }
    });

    $.ajax({
        url: site_url + "amc/chartuntung",
        method: "GET",
        dataType: "JSON",
        success: function(data) {
            var januari = [];
            var februari = [];
            var maret = [];
            var april = [];
            var mei = [];
            var juni = [];
            var juli = [];
            var agustus = [];
            var september = [];
            var oktober = [];
            var november = [];
            var desember = [];
            var d = new Date();
            var n = d.getFullYear();
            for (var i in data) {
                januari.push(Number(data[i].keluarjanuari));
                februari.push(Number(data[i].keluarfebruari));
                maret.push(Number(data[i].keluarmaret));
                april.push(Number(data[i].keluarapril));
                mei.push(Number(data[i].keluarmei));
                juni.push(Number(data[i].keluarjuni));
                juli.push(Number(data[i].keluarjuli));
                agustus.push(Number(data[i].keluaragustus));
                september.push(Number(data[i].keluarseptember));
                oktober.push(Number(data[i].keluaroktober));
                november.push(Number(data[i].keluarnovember));
                desember.push(Number(data[i].keluardesember));
            }
            var cok = [januari, februari, maret, april, mei, juni, juli, agustus, september, oktober, november, desember];

            Highcharts.chart('container5', {
                chart: {
                    type: 'area'
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    enabled: false
                },

                title: {
                    text: 'Barang Terjual'
                },
                xAxis: {
                    categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli',
                        'Agustus', 'September', 'Oktober', 'November', 'Desember'
                    ]
                },
                credits: {
                    enabled: false
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Pcs'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' +
                        ' <td style="padding:0"> <b> {point.y}  Pcs</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    },
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },
                series: [{
                    name: 'Terjual',
                    data: cok,
                    color: '#0066ff'
                }]
            });

        }
    });

    var detailObj = {};

    var arrDatabrg = {};
    var arrDatakat = {};
    var arrDatajum = {};
    var arrDataharga = {};
    var arrDataket = {};
    var arrDatatot = {};
    var Obj = new Array();
    Obj.push(arrDatajum);
    Obj.push(arrDataharga);
    Obj.push(arrDatatot);


    $('body').on('click', '.simpan_keluar', function(e) {
        var id = $('#barangkeluar').val();
        var jumlah = $('#jumkeluar').val();
        var qty = $('#stok').val();
        var hargajual = $('#hargajual').val();
        var tanggal = $('#datepicker').val();
        var nama = $('#namapengambil').val();
        var keterangan = $('#keterangan').val();

        var total = '';

        if (jumlah == '' || tanggal == '' || nama == '' || id == '' || hargajual == '' || keterangan == '') {
            swal({
                title: "Error!",
                text: "Harus di isi !!",
                type: "warning",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            var sumtotal = 0;
            var sumjumlah = 0;
            var sumharga = 0;
            $.ajax({
                type: 'POST',
                url: site_url + "amc/vwkeluar",
                data: { id: id },
                dataType: "JSON",
                success: function(data, status) {

                    for (i in data) {
                        detail_idx = Object.keys(detailObj).length;
                        detailObj[detail_idx] = (data);
                        total = jumlah * hargajual;

                        $('#tb_pembelian').append('<tr data-idx="' + detail_idx + '"> <td class="kode">' + data[i].kodebarang + '</td>' +
                            '<td class="nama">' + data[i].namabarang + '</td> <td class="serial">' + data[i].serialnumber + '</td>' +
                            '<td class"jumlah">' + jumlah + '</td> <td class="hargajual">' + hargajual + '</td>  <td>' + total + '</td> <td>' + keterangan + '</td>' +
                            // '<td> <button class="btn btn-sm btn-danger del_pem"><i class="fa fa-trash-alt"></i></button></td>'+
                            '</tr>');
                        $(".bt_simpan_entry").removeAttr('disabled');

                        arrDatabrg[detail_idx] = (data[i].idbarang);
                        arrDatakat[detail_idx] = (data[i].idkategori);
                        arrDatajum[detail_idx] = (jumlah);
                        arrDataharga[detail_idx] = (hargajual);
                        arrDataket[detail_idx] = (keterangan);
                        arrDatatot[detail_idx] = (total);
                    }
                    $.each(arrDatatot, function() { sumtotal += parseFloat(this) || 0; });
                    $('[name="totalkeluar"]').val(sumtotal);


                    // console.log(sumharga);

                    if (status == 'success') {
                        $('#barangkeluar').val(null).trigger('change');
                        $('#jumkeluar').val('');
                        $('#hargajual').val('');
                        $('#stok').val('');
                        $('#keterangan').val('');

                    }


                }
            })
        }

    });


    /*simpan entry*/
    $('body').on('click', '.bt_simpan_entry', function(e) {

        // dt_pend = $('form#pembelian').serializeArray();
        total = $('#totalkeluar').val();
        bayar = $('#bayarkeluar').val();
        sisa = $('#sisakeluar').val();
        tgljual = $('#datepicker').val();
        namapengambil = $('#namapengambil').val();

        if (bayar == '' || sisa == '') {
            swal({
                title: "Error!",
                text: "Harus di isi !!",
                type: "warning",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                url: site_url + "amc/simpan",
                type: 'POST',
                dataType: 'json',
                data: {
                    // master: dt_pend,
                    detail: detailObj,
                    idbarang: arrDatabrg,
                    idkategori: arrDatakat,
                    jumlah: arrDatajum,
                    hargajual: arrDataharga,
                    ket: arrDataket,
                    total: total,
                    bayar: bayar,
                    sisa: sisa,
                    tgljual: tgljual,
                    namapengambil: namapengambil,
                },
                success: function(data, status) {
                    // toastr[data.alert](data.data, data.title);

                    if (status == 'success') {
                        swal({
                            title: "Success!",
                            text: "Berhasil Di Simpan.",
                            type: "success",
                            showCancelButton: false,
                            showConfirmButton: false,
                            timer: 3000
                        }, function() {
                            swal.close();
                            $('#modal-inp-keluar').modal('hide');
                            location.reload();
                        });


                    }

                }
            })
        }

    });

    $('body').on('click', '.del_pem', function(e) {
        idx_key = $(this).closest('tr').data('idx');
        console.log('delete');
        delete detailObj[idx_key];
        $(this).closest('tr').remove();
        return false;

    });

    $('body').on('click', '.detailkeluar', function(e) {
        e.preventDefault();

        var kode = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/detailpembeliam",
            data: { kode: kode },
            dataType: "JSON",
            success: function(data) {

                if ($.fn.DataTable.isDataTable('#detail_rpt01')) {
                    $('#detail_rpt01').DataTable().destroy();
                }
                $('#detail_rpt01').DataTable(data);
                $('#modal_form_layanan01').modal('show');

            }
        });
    });

    $('body').on('click', '.detailangsuran', function(e) {
        e.preventDefault();

        var kode = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/detailangsuran",
            data: { kode: kode },
            dataType: "JSON",
            success: function(data) {

                if ($.fn.DataTable.isDataTable('#detail_rpt02')) {
                    $('#detail_rpt02').DataTable().destroy();
                }
                $('#detail_rpt02').DataTable(data);
                $('#modal_form_layanan02').modal('show');

            }
        });
    });


    $('.content-wrapper').on('click', '.hapuskeluar', function(e) {
        e.preventDefault();
        var idtrans = $(this).data('kode');
        swal({
            title: 'Delete Data',
            text: 'Yakin Ingin Menghapus Data ?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        }, function() {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/deletekeluar",
                data: { idtrans: idtrans },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    keluar.ajax.reload(null, false);
                }
            })
        })
    });

    $('.content-wrapper').on('click', '.pdfpembelian', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        window.open(site_url + "amc/pdfpembelian/" + id, '_blank');
    });

    $('.content-wrapper').on('click', '.pdfkeluar', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        window.open(site_url + "amc/pdfpengeluaran/" + id, '_blank');
    });

    $('.content-wrapper').on('click', '.pdfangsuran', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        window.open(site_url + "amc/pdfangsuran/" + id, '_blank');
    });

    $('body').on('click', '.detaildinas', function(e) {
        e.preventDefault();

        var kode = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/dinasdetail",
            data: { kode: kode },
            dataType: "JSON",
            success: function(data) {

                if ($.fn.DataTable.isDataTable('#detail_dinas')) {
                    $('#detail_dinas').DataTable().destroy();
                }
                $('#detail_dinas').DataTable(data);
                $('#modaldetail').modal('show');

            }
        });
    });

    $('.content-wrapper').on('click', '.simpan_dinas', function(e) {
        e.preventDefault();
        var tgldinas = $('#datepicker').val();
        var tujuan = $('#tujuan').val();
        var keperluan = $('#keperluan').val();
        if (tgldinas == '' || tujuan == '' || keperluan == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertdinas",
                data: {
                    tgldinas: tgldinas,
                    tujuan: tujuan,
                    keperluan: keperluan
                },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-inp-dinas').modal('hide');
                    $('#datepicker').val('');
                    $('#tujuan').val('');
                    $('#keperluan').val('');
                    tbdinas.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.editdinas', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editdinas",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="iddinas"]').val(data[i].id);
                    $('[name="edittujuan"]').val(data[i].tujuan);
                    $('[name="edittgldinas"]').val(data[i].tanggal);
                    $('[name="editkeperluan"]').val(data[i].keperluan);
                    $('#modal-edit-dinas').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_edit_dinas', function(e) {
        e.preventDefault();
        var id = $('#iddinas').val();
        var tgldinas = $('#datepicker1').val();
        var tujuan = $('#edittujuan').val();
        var keperluan = $('#editkeperluan').val();
        if (id == '' || tgldinas == '' || tujuan == '' || keperluan == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updatedinas",
                data: {
                    tgldinas: tgldinas,
                    tujuan: tujuan,
                    keperluan: keperluan,
                    id: id
                },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-dinas').modal('hide');
                    $('#iddinas').val('');
                    $('#datepicker1').val('');
                    $('#edittujuan').val('');
                    $('#editkeperluan').val('');
                    tbdinas.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapusdinas', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        swal({
            title: 'Delete Data',
            text: 'Yakin Ingin Menghapus Data ?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        }, function() {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/deletedinas",
                data: { id: id },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    tbdinas.ajax.reload(null, false);
                }
            })
        })
    });

    $('.content-wrapper').on('click', '.ekspedisi', function(e) {
        e.preventDefault();
        var id = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editdinas",
            data: { id: id },
            dataType: "JSON",
            success: function(data) {
                for (i in data) {
                    $('[name="iddinas"]').val(data[i].id);
                    $('[name="tujuan"]').val(data[i].tujuan);
                    $('[name="tgldinas"]').val(data[i].tanggal);
                    $('[name="keperluan"]').val(data[i].keperluan);
                    $('#modal-inp-ekspedisi').modal('show');
                }
            }
        })
    });

    var detailObj = {};

    var arrDataId = {};
    var arrDataEkdpedisi = {};
    var arrDataTujuan = {};
    var arrDataHarga = {};
    var arrDataTanggal = {};
    var Obj = new Array();
    Obj.push(arrDataId);

    $('body').on('click', '.simpan_ekspedisi', function(e) {
        var id = $('#iddinas').val();
        var ekspedisi = $('#ekspedisi').val();
        var tujuan = $('#tujuanekspedisi').val();
        var harga = $('#hargapengeluaran').val();
        var tanggal = $('#datepicker2').val();

        if (ekspedisi == '' || tanggal == '' || tujuan == '' || id == '' || harga == '' || tanggal == '') {
            swal({
                title: "Error!",
                text: "Harus di isi !!",
                type: "warning",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/get_editdinas",
                data: { id: id },
                dataType: "JSON",
                success: function(data, status) {

                    for (i in data) {
                        detail_idx = Object.keys(detailObj).length;
                        detailObj[detail_idx] = (data);

                        $('#tb_ekspedisi').append('<tr data-idx="' + detail_idx + '"> <td class="kode">' + ekspedisi + '</td>' +
                            '<td class="nama">' + tujuan + '</td> <td class="serial">' + harga + '</td>' +
                            '<td class"jumlah">' + tanggal + '</td>' +
                            '</tr>');
                        $(".bt_simpan_ekspedisi").removeAttr('disabled');

                        arrDataId[detail_idx] = (data[i].id);
                        arrDataEkdpedisi[detail_idx] = (ekspedisi);
                        arrDataTujuan[detail_idx] = (tujuan);
                        arrDataHarga[detail_idx] = (harga);
                        arrDataTanggal[detail_idx] = (tanggal);
                    }

                    if (status == 'success') {
                        $('#ekspedisi').val('');
                        $('#tujuanekspedisi').val('');
                        $('#hargapengeluaran').val('');
                        $('#datepicker2').val('');

                    }


                }
            })
        }

    });

    $('body').on('click', '.bt_simpan_ekspedisi', function(e) {
        console.log(arrDataId);
        $.ajax({
            url: site_url + "amc/simpanekspedisi",
            type: 'POST',
            dataType: 'json',
            data: {
                id: arrDataId,
                ekspedisi: arrDataEkdpedisi,
                harga: arrDataHarga,
                tujuan: arrDataTujuan,
                tanggal: arrDataTanggal
            },
            success: function(data, status) {
                console.log(status);
                if (status == 'success') {
                    swal({
                        title: "Success!",
                        text: "Berhasil Di Simpan.",
                        type: "success",
                        showCancelButton: false,
                        showConfirmButton: false,
                        timer: 2000
                    }, function() {
                        swal.close();
                        $('#modal-inp-ekspedisi').modal('hide');
                        location.reload();
                    });


                }

            }
        })

    });

    // Rak
    $('.content-wrapper').on('click', '.editrak', function(e) {
        e.preventDefault();
        var idrak = $(this).data('kode');
        $.ajax({
            type: 'POST',
            url: site_url + "amc/get_editrak",
            data: { idrak: idrak },
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                for (i in data) {
                    $('[name="idrak"]').val(data[i].id);
                    $('[name="editnamarak"]').val(data[i].namarak);
                    $('#modal-edit-rak').modal('show');
                }
            }
        })
    });

    $('.content-wrapper').on('click', '.simpan_rak', function(e) {
        e.preventDefault();
        var namarak = $('#namarak').val();
        console.log(namarak);
        if (namarak == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/insertrak",
                data: { namarak: namarak },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-rak').modal('hide');
                    rak.ajax.reload(null, false);
                    $('#namarak').val('');
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.simpan_rak_edit', function(e) {
        e.preventDefault();
        var idrak = $('#idrak').val();
        var namarak = $('#editnamarak').val();
        if (idrak == '' || namarak == '') {
            swal({
                title: "Error!",
                text: "Semua Harus di isi !!",
                type: "error",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 2000
            }, function() {
                swal.close();
            });

            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/updaterak",
                data: { idrak: idrak, namarak: namarak },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    $('#modal-edit-rak').modal('hide');
                    $('#editnamarak').val('');
                    rak.ajax.reload(null, false);
                }
            })
        }
    });

    $('.content-wrapper').on('click', '.hapusrak', function(e) {
        e.preventDefault();
        var idrak = $(this).data('kode');
        swal({
            title: 'Delete Data',
            text: 'Yakin Ingin Menghapus Data ?',
            html: true,
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        }, function() {
            $.ajax({
                type: 'POST',
                url: site_url + "amc/deleterak",
                data: { idrak: idrak },
                dataType: "JSON",
                success: function(data) {
                    console.log(data);
                    toastr[data.alert](data.data, data.title);
                    rak.ajax.reload(null, false);
                }
            })
        })
    });

});

$('#lappdfmasuk').click(function() {
    $('#ipladder').attr('target', '_blank');
    $('#ipladder').attr('action', site_url + "amc/pdfmasuklaporan");
    $('#ipladder').submit();
});

$('#lappdfkeluar').click(function() {
    $('#ipladder1').attr('target', '_blank');
    $('#ipladder1').attr('action', site_url + "amc/pdfkeluarlaporan");
    $('#ipladder1').submit();
});

$('#cetakstokbrg').click(function() {
    $('#ipladder2').attr('target', '_blank');
    $('#ipladder2').attr('action', site_url + "amc/pdfstokbarang");
    $('#ipladder2').submit();
});
$('#laprekaplaba').click(function() {
    $('#formlaba').attr('target', '_blank');
    $('#formlaba').attr('action', site_url + "amc/pdfrekaplaba");
    $('#formlaba').submit();
});


function tambah() {
    // window.open(site_url + "amc/indexkeluar");
    window.location.href = site_url + "amc/indexkeluar";
}

function number_format(number, decimals, decPoint, thousandsSep) {
    decimals = decimals || 0;
    number = parseFloat(number);

    if (!decPoint || !thousandsSep) {
        decPoint = '.';
        thousandsSep = ',';
    }

    var roundedNumber = Math.round(Math.abs(number) * ('1e' + decimals)) + '';
    var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
    var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
    var formattedNumber = "";

    while (numbersString.length > 3) {
        formattedNumber += thousandsSep + numbersString.slice(-3)
        numbersString = numbersString.slice(0, -3);
    }

    return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
}