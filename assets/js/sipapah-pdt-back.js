jQuery(document).ready(function($) {
	var site_url =$('body').data('site_url');
	console.log (site_url);
	$("#simpan_pdt").attr('disabled', 'disabled');
	$("#simpan_pdt").removeClass('btn-success');
	//$("#simpan_pdt").addClass('btn-outline-success')
	$("#nopel").focus();
	

	//var nopel='';
	//nopel= 
	$('#getPelayanan').on('click', function(e) {
        e.preventDefault();
        //var id = $(this).find(':selected')[0].id;
        var id = $("#nopel").val();
        //alert(id);
        $.ajax({
            type: 'POST',
            url: site_url + "pendataan/get_pelayanan/",
            data: {
                  'nopen': id
            },
            dataType: "JSON",
            success: function(data) {
                // the next thing you want to do
                console.log(data);
                var konten = $('.postx');
                konten.empty();
                
               
                for (var i = 0; i < data.length; i++) {
                    konten.append( 
                             '<div class="form-group">'+
                  				      '<div class="row">'+
                  				        '<div class="col-9"><label><span>' + data[i].NM_JENIS_PELAYANAN + '</span>'+
                  				          ' a.n '+data[i].NAMA_TERMOHON + '</label></div>'+
                				            '<div class="col-1"><a href="#" class="btn btn-sm btn-info float-right bt_detail_permohonan" data-toggle="modal" data-nopen-detail="' + data[i].NOPEN_DETAIL +'" data-kode="' + data[i].KODE +'" data-target="#modal-pendataan"><i class="fa fa-search"></i> Detail</a></div><div class="col-2">'+
                  				          '<input type="checkbox" class="float-right status dt_status" name="status" '+
                  				          'data-nopen="' + data[i].NOPEN_DETAIL +
                  				          '" data-toggle="toggle" data-on="Terima" data-off="Tolak" data-onstyle="success" data-offstyle="danger" data-size="small" data-width="110">'+
					                         '</div></div><div class="row"><div class="input-group"><div class="input-group-prepend">'+
					                   '<span class="input-group-text"><i class="fas fa-comment"></i></span></div>'+
					                   '<input class="form-control ket_detail" name="ket_detail"  placeholder="Catatan" type="text" value="' + data[i].KET_DETAIL + '">'+
					                   '</div></div></div>');
                }
                //manually trigger a change event for the contry so that the change handler will get triggered
               // console.log( konten );
                  $(".dt_status").bootstrapToggle();
                  $("#simpan_pdt").removeAttr('disabled');
                  $("#simpan_pdt").removeClass('btn-outline-success');
                  $("#simpan_pdt").addClass('btn-success')
            }
        });
    });


$('body').on('click', '.bt_detail_permohonan' ,function(e) {
       console.log('');
        
       // var id = $(this).find(':selected')[0].id;
        var kode = $(this).data('kode');
        var nopendetail = $(this).data('nopen-detail');
        var jns_layanan = $(this).closest('.row').find('label>span').text();

        //alert(id);
        $.ajax({
            type: 'POST',
            url: site_url + "pendataan/get_permohonan"+kode+"/",
            data: {
                  'id': nopendetail
            },
          //  dataType: "JSON",
            
      success: function(html) {
        $(".modal-append").html("");
        $(".modal-append").append(html);
        $(".modal-subtitle").html(jns_layanan);
        $(".modal-append input").prop('readonly', true);

      }
            
        });
          
      e.preventDefault();
    });

	
	





	
	
	/* validation*/
	  $( ".nik" ).on('input', function() { 
                var value=$(this).val().replace(/[^0-9]*/g, '');
                $(this).val(value)

        });


	  $( ".numerical" ).on('input', function() { 
              var value =$(this).val().replace(/[^0-9]*/g, '');
                value   = value.replace(/\.{2,}/g, '.');
                value   = value.replace(/\.,/g, ',');
                value   = value.replace(/\,\./g, ',');
                value   = value.replace(/\,{2,}/g, ',');
                value   = value.replace(/\.[0-9]+\./g, '.');
                $(this).val(value)

        });

  	$( ".no_telp" ).on('input', function() { 
                var value=$(this).val().replace(/[^0-9.,+,-]*/g, '');
                $(this).val(value)

        });

  	$( ".alow_text" ).on('input', function() { 
                var value=$(this).val().replace(/[^0-9.,-,a-z,A-Z,']*/g, '');
                $(this).val(value)

        });

		/* simpan Hasil Verifikasi Pendaftaran*/

    $('body').on('click', '#simpan_pdt', function(event) {
        //cetak_url =  $(this).data('cetak');
        proses_url = $(this).data('verifikasi');
       // catatan = $('#catatan').val();
        no_pend = $('#nopel').val();
        //var no_hp = $('.no_hp').val();
		

        var arrData = [];
        // loop over each table row (tr)
        $("form .postx").each(function() {
        	console.log($(this).find('.status').data('nopen'));
            var obj = {};
            obj.no_pend_detail = $(this).find('.status').data('nopen');
           // obj.no_urut = $(this).find('.no_urut').val();
           if($(this).find('.status:checked').val()=='on')
           {
           	obj.status_permohonan='2';
           }
           else
           {
           	obj.status_permohonan='-2';
           }
           // obj.status_permohonan = $(this).find('.status:checked').val();
            obj.ket_detail = $(this).find('.ket_detail').val();
            //obj.no_hp = $(this).find('.no_hp').val();
            //obj.nama        = nama;

            arrData.push(obj);
        });

        console.log(arrData);
        //console.log(no_pend);
        //console.log(catatan);
        $.ajax({
                url: proses_url,
                type: 'POST',
                dataType: 'json',
                data: {
                   // master: catatan,
                    detail: arrData,
                   no_pend: no_pend
					
                },
            })
            .done(function(data) {

                //console.log(data.alert);
				toastr[data.alert](data.data, data.title);
				$("#simpan_pdt").attr('disabled', 'disabled');
				$('.postx').empty();
				$("#simpan_pdt").removeClass('btn-success');
				$("#simpan_pdt").addClass('btn-outline-success')
                $("#nopel").val('');
                $("#nopel").focus();
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
			

       // window.open(cetak_url+'/'+no_pend, '_blank');
        //window.location.href = site_url + '/pendataan/index';
        event.preventDefault();

    });

    histori_layanan = $('#tb_histori').DataTable({

		// "scrollX": true,
		// "processing": true, 
		// "serverSide": true, 
		// "order": [],
		"searching": false,
		"ordering": false,
        "info":     false, 
        "lengthChange":true,
        "pageLength": 5,
		// Load data for the table's content from an Ajax source
		"ajax": {
			//"url": "<?php echo site_url('tahun_ajaran/ajax_list')?>",
			"url": site_url + "pelayanan/ajax_list_histori",
			"type": "POST"
		},

		//Set column definition initialisation properties.
		
	});

	 $('#tb_histori_length').css({ opacity: 0 });

})

Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}

