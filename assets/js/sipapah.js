jQuery(document).ready(function ($) {

	$('#process').ajaxStart(function () {
		$(this).fadeIn();

	}).ajaxStop(function () {
		$(this).fadeOut();
	})

	$('#datepicker').datepicker({
		autoclose: true,
		format: "yyyy-mm-dd",
	})

	$('#datepicker1').datepicker({
		autoclose: true,
		format: "yyyy-mm-dd",
	})

	$('#datepickerkec').datepicker({
		autoclose: true,
		format: "yyyy-mm-dd",
	})

	$('#datepickerkec1').datepicker({
		autoclose: true,
		format: "yyyy-mm-dd",
	})

	$('#datepicker1').change(function () {
		var start = $('#datepicker').val();
		var end = $('#datepicker1').val();
		$('#process').css('display', 'block');
		$.ajax({
			url: site_url + "dhkpdesa/ajaxrealisasidesa",
			method: "POST",
			data: { start: start, end: end },
			async: true,
			dataType: 'json',
			success: function (data) {
				$('#process').css('display', 'none');
				if ($.fn.DataTable.isDataTable('#realisasidesa')) {
					$('#realisasidesa').DataTable().destroy();
				}

				$('#realisasidesa').DataTable(data);
			}
		});
		return false;
	});

	$('#datepickerkec1').change(function () {
		var start = $('#datepickerkec').val();
		var end = $('#datepickerkec1').val();
		$('#process').css('display', 'block');
		// $('#process').text('Loading...');
		$.ajax({
			url: site_url + "dhkpkec/ajaxrealisasikec",
			method: "POST",
			data: { start: start, end: end },
			async: true,
			dataType: 'json',
			success: function (data) {
				$('#process').css('display', 'none');
				if ($.fn.DataTable.isDataTable('#realisasikec')) {
					$('#realisasikec').DataTable().destroy();
				}

				$('#realisasikec').DataTable(data);
			}
		});
		return false;
	});

	$('#kec').change(function () {
		var kec = $(this).val();
		$.ajax({
			url: site_url + "dhkp/refdesa",
			method: "POST",
			data: { kec: kec },
			async: true,
			dataType: 'json',
			success: function (data) {
				// console.log(data);
				var html = '';
				var i;
				html = '<option value="">--- Pilih Kalurahan ---</option>';
				for (i = 0; i < data.length; i++) {
					html += '<option value=' + data[i].KD_KELURAHAN + '>' + data[i].NM_KELURAHAN + '</option>';
				}
				$('#desa').html(html);
			}
		});
		return false;
	});

	$('#status').change(function () {
		var tahun = $('#tahun').val();
		var kec = $('#kec').val();
		var desa = $('#desa').val();
		var status = $(this).val();
		$('#process').css('display', 'block');
		$.ajax({
			url: site_url + "dhkp/ajax_dhkp_detail",
			method: 'post',
			data: { tahun: tahun, kec: kec, desa: desa, status: status },
			async: true,
			dataType: 'json',
			success: function (data) {
				$('#process').css('display', 'none');
				if ($.fn.DataTable.isDataTable('#dhkp_detail')) {
					$('#dhkp_detail').DataTable().destroy();
				}
				$('#dhkp_detail').DataTable(data);
			}
		});
		return false;
	});

	$('#statusdesa').change(function () {
		var statusdesa = $(this).val();
		$('#process').css('display', 'block');
		$.ajax({
			url: site_url + "dhkpdesa/ajaxdhkpdesastatus",
			method: 'post',
			data: { statusdesa: statusdesa },
			async: true,
			dataType: 'json',
			success: function (data) {
				$('#process').css('display', 'none');
				if ($.fn.DataTable.isDataTable('#dhkptable')) {
					$('#dhkptable').DataTable().destroy();
				}
				$('#dhkptable').DataTable(data);
			}
		});
		return false;
	});

	$('#kecrealisasi').change(function () {
		var kec = $(this).val();
		$.ajax({
			url: site_url + "dhkp/refdesa",
			method: "POST",
			data: { kec: kec },
			async: true,
			dataType: 'json',
			success: function (data) {
				var html = '';
				var i;
				html = '<option value="">--- Pilih Kalurahan ---</option>';
				for (i = 0; i < data.length; i++) {
					html += '<option value=' + data[i].KD_KELURAHAN + '>' + data[i].NM_KELURAHAN + '</option>';
				}
				$('#desarealisasi').html(html);
			}
		});
		return false;
	});

	$('#desarealisasi').change(function () {
		var kec = $('#kecrealisasi').val();
		var desa = $('#desarealisasi').val();
		$('#process').css('display', 'block');
		$.ajax({
			url: site_url + "dhkp/ajaxrealisasidetail",
			method: 'post',
			data: { kec: kec, desa: desa },
			async: true,
			dataType: 'json',
			success: function (data) {
				$('#process').css('display', 'none');
				if ($.fn.DataTable.isDataTable('#realisasidetail')) {
					$('#realisasidetail').DataTable().destroy();
				}
				$('#realisasidetail').DataTable(data);
			}
		});
		return false;
	});
	$('#kecperalihan').change(function () {
		var kec = $(this).val();
		$.ajax({
			url: site_url + "dhkp/refdesa",
			method: "POST",
			data: { kec: kec },
			async: true,
			dataType: 'json',
			success: function (data) {
				var html = '';
				var i;
				html = '<option value="">--- Pilih Kalurahan ---</option>';
				for (i = 0; i < data.length; i++) {
					html += '<option value=' + data[i].KD_KELURAHAN + '>' + data[i].NM_KELURAHAN + '</option>';
				}
				$('#desaperalihan').html(html);
			}
		});
		return false;
	});

	$('#desaperalihan').change(function () {
		var kec = $('#kecperalihan').val();
		var desa = $('#desaperalihan').val();
		$('#process').css('display', 'block');
		$.ajax({
			url: site_url + "dhkp/ajaxperalihan",
			method: 'post',
			data: { kec: kec, desa: desa },
			async: true,
			dataType: 'json',
			success: function (data) {
				$('#process').css('display', 'none');
				if ($.fn.DataTable.isDataTable('#peralihanadmin')) {
					$('#peralihanadmin').DataTable().destroy();
				}
				$('#peralihanadmin').DataTable(data);
			}
		});
		return false;
	});

	$('.content-wrapper').on('click', '.detail', function (e) {
		e.preventDefault();
		var id = $(this).data('id');
		$.ajax({
			type: 'POST',
			url: site_url + "dhkpdesa/peralihandetail",
			data: { id: id },
			dataType: "JSON",
			success: function (data) {
				for (i in data.data) {
					$('[name="tgl"]').val(data.data[i].t_tglprosesspt);
					$('[name="status"]').val(data.data[i].s_namajenistransaksi);
					$('[name="notaris"]').val(data.data[i].s_namanotaris);
					$('[name="nop"]').val(data.data[i].t_nopbphtbsppt);
					$('[name="namapenjual"]').val(data.data[i].t_namawppenjual);
					$('[name="alamatpenjual"]').val(data.data[i].t_alamatwppenjual + ' RT. ' + data.data[i].t_kelurahanwppenjual + ' ' + data.data[i].t_kecamatanwppenjual + ' ' + data.data[i].t_kabkotawppenjual);
					$('[name="namapembeli"]').val(data.data[i].t_namawppembeli);
					$('[name="alamatpembeli"]').val(data.data[i].t_alamatwppembeli + ' RT. ' + data.data[i].t_rtwppembeli + ' RW. ' + data.data[i].t_rwwppembeli + ' ' + data.data[i].t_kelurahanwppembeli + ' ' + data.data[i].t_kecamatanwppembeli + ' ' + data.data[i].t_kabkotawppembeli);
					$('[name="luastanah"]').val(data.data[i].t_luastanah);
					$('[name="luasbangunan"]').val(data.data[i].t_luasbangunan);
					$('#modal_form').modal('show');
				}
			}
		});
	});

	$('#pel').DataTable({
		"paging": false,
		"ordering": false,
		"info": false,
		"searching": false,
		"ajax": {
			"url": site_url + "dhkp/ajax_pelayanan",
			"type": "POST"
		},

	});

	$('#realisasi').DataTable({

		"ajax": {
			"url": site_url + "dhkp/ajaxrealisasi",
			"type": "POST"
		},

	});

	$('#dhkptable').DataTable({

		"ajax": {
			"url": site_url + "dhkpdesa/ajaxdhkpdesa",
			"type": "POST"
		},

	});

	$('#datepicker1').change()

	$('#peralihan').DataTable({

		"ajax": {
			"url": site_url + "dhkpdesa/ajaxperalihandesa",
			"type": "POST"
		},

	});

	$('#dhkpkec').DataTable({

		"ajax": {
			"url": site_url + "dhkpkec/ajaxdhkpkec",
			"type": "POST"
		},

	});

	$('#dhkpadmin').DataTable({

		"ajax": {
			"url": site_url + "dhkp/ajax_dhkp_admin",
			"type": "POST"
		},

	});

	$('#realisasi_table').DataTable({
		"paging": true,
		"ordering": true,
		"info": true
	});

	$.ajax({
		url: site_url + "dhkpdesa/chartrealisasi",
		method: "GET",
		dataType: "JSON",
		success: function (data) {
			var label = [];
			var value = [];
			for (var i in data) {
				// var real = rubah(data[i].JUMBAYAR);
				// var bulan = data[i].BULAN; 
				// label.push(bulan);
				// console.log(value.push(real));

				var jml = data[i].JUMBAYAR;
				label.push(data[i].BULAN);
				value.push(jml);
				// console.log(jml);
			}

			var realisasiChartData = {
				labels: label,
				datasets: [
					{
						label: 'Realisasi',
						backgroundColor: 'rgba(157, 21, 255, 0.62)',
						borderColor: 'rgba(0, 0, 189, 1)',
						pointRadius: false,
						pointColor: '#3b8bba',
						pointStrokeColor: 'rgba(157, 21, 255, 1)',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(157, 21, 255, 1)',
						data: value
					},
				]
			}

			var areaChartOptions = {
				maintainAspectRatio: false,
				responsive: true,
				legend: {
					display: true
				},
				scales: {
					xAxes: [{
						gridLines: {
							display: true,
						}
					}],
					yAxes: [{
						gridLines: {
							display: true,
						}
					}]
				}
			}

			var lineChartCanvas = $('#areaChart').get(0).getContext('2d')
			var lineChartOptions = jQuery.extend(true, {}, areaChartOptions)
			var lineChartData = jQuery.extend(true, {}, realisasiChartData)
			lineChartData.datasets[0].fill = false;
			lineChartOptions.datasetFill = false;

			var lineChart = new Chart(lineChartCanvas, {
				type: 'bar',
				data: lineChartData,
				options: lineChartOptions
			})

			// var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
			// var areaChart = new Chart(areaChartCanvas, { 
			// 	type: 'line',
			// 	data: realisasiChartData, 
			// 	options: areaChartOptions
			// })
		}
	})

	$.ajax({
		url: site_url + "dhkpdesa/chartketetapan",
		method: "GET",
		dataType: "JSON",
		success: function (data) {
			var label = [];
			var value = [];
			var ketetapan = [];
			for (var i in data) {
				label.push(data[i].THNPAJAK);
				ketetapan.push(data[i].KETETAPAN);
				value.push(data[i].REALISASI);
			}

			var areaChartData = {
				labels: label,
				datasets: [
					{
						label: 'Ketetapan',
						backgroundColor: 'rgba(60,141,188,0.9)',
						borderColor: 'rgba(60,141,188,0.8)',
						pointRadius: false,
						pointColor: '#3b8bba',
						pointStrokeColor: 'rgba(60,141,188,1)',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(60,141,188,1)',
						data: ketetapan
					},
					{
						label: 'Realisasi',
						backgroundColor: 'rgba(210, 214, 222, 1)',
						borderColor: 'rgba(210, 214, 222, 1)',
						pointRadius: false,
						pointColor: 'rgba(210, 214, 222, 1)',
						pointStrokeColor: '#c1c7d1',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(220,220,220,1)',
						data: value
					},
				]
			}
			var barChartOptions = {
				responsive: true,
				maintainAspectRatio: false,
				datasetFill: true
			}


			var barChartCanvas = $('#barChart').get(0).getContext('2d')

			var barChart = new Chart(barChartCanvas, {
				type: 'bar',
				data: areaChartData,
				options: barChartOptions
			})
		}
	})

	$.ajax({
		url: site_url + "dhkpkec/chartrealisasikec",
		method: "GET",
		dataType: "JSON",
		success: function (data) {
			var label = [];
			var value = [];
			for (var i in data) {
				// var real = rubah(data[i].JUMBAYAR);
				// var bulan = data[i].BULAN; 
				// label.push(bulan);
				// console.log(value.push(real));

				var jml = data[i].REALISASI;
				label.push(data[i].DESA);
				value.push(jml);
				console.log(jml);
			}

			var realisasiChartData = {
				labels: label,
				datasets: [
					{
						label: 'Realisasi',
						backgroundColor: 'rgba(157, 21, 255, 0.62)',
						borderColor: 'rgba(0, 0, 189, 1)',
						pointRadius: false,
						pointColor: '#3b8bba',
						pointStrokeColor: 'rgba(157, 21, 255, 1)',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(157, 21, 255, 1)',
						data: value
					},
				]
			}

			var areaChartOptions = {
				maintainAspectRatio: false,
				responsive: true,
				legend: {
					display: true
				},
				scales: {
					xAxes: [{
						gridLines: {
							display: true,
						}
					}],
					yAxes: [{
						gridLines: {
							display: true,
						}
					}]
				}
			}

			var lineChartCanvas = $('#lineChartKec').get(0).getContext('2d')
			var lineChartOptions = jQuery.extend(true, {}, areaChartOptions)
			var lineChartData = jQuery.extend(true, {}, realisasiChartData)
			lineChartData.datasets[0].fill = false;
			lineChartOptions.datasetFill = false

			var lineChart = new Chart(lineChartCanvas, {
				type: 'bar',
				data: lineChartData,
				options: lineChartOptions
			})

			// var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
			// var areaChart = new Chart(areaChartCanvas, { 
			// 	type: 'line',
			// 	data: realisasiChartData, 
			// 	options: areaChartOptions
			// })
		}
	})

	$.ajax({
		url: site_url + "dhkpkec/chartketetapankec",
		method: "GET",
		dataType: "JSON",
		success: function (data) {
			var label = [];
			var value = [];
			var ketetapan = [];
			for (var i in data) {
				label.push(data[i].THNPAJAK);
				value.push(data[i].REALISASI);
				ketetapan.push(data[i].KETETAPAN);
			}

			var areaChartData = {
				labels: label,
				datasets: [
					{
						label: 'Ketetapan',
						backgroundColor: 'rgba(60,141,188,0.9)',
						borderColor: 'rgba(60,141,188,0.8)',
						pointRadius: false,
						pointColor: '#3b8bba',
						pointStrokeColor: 'rgba(60,141,188,1)',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(60,141,188,1)',
						data: ketetapan
					},
					{
						label: 'Realisasi',
						backgroundColor: 'rgba(210, 214, 222, 1)',
						borderColor: 'rgba(210, 214, 222, 1)',
						pointRadius: false,
						pointColor: 'rgba(210, 214, 222, 1)',
						pointStrokeColor: '#c1c7d1',
						pointHighlightFill: '#fff',
						pointHighlightStroke: 'rgba(220,220,220,1)',
						data: value
					},
				]
			}
			var barChartOptions = {
				responsive: true,
				maintainAspectRatio: false,
				datasetFill: false
			}


			var barChartCanvas = $('#ChartKec').get(0).getContext('2d')

			var barChart = new Chart(barChartCanvas, {
				type: 'bar',
				data: areaChartData,
				options: barChartOptions
			})
		}
	})
})

function pdfdhkpkec() {
	window.open(site_url + "dhkpkec/mpdfkec", '_blank');
}

function exceldhkpkec() {
	window.open(site_url + "dhkpkec/excelkec", '_blank');
}

function pdfperalihandesa() {
	window.open(site_url + "dhkpdesa/mpdfperalihandesa", '_blank');
}

function excelperalihandesa() {
	window.open(site_url + "dhkpdesa/excelperalihandesa", '_blank');
}

function pdfdhkpadmin() {
	window.open(site_url + "dhkp/mpdfadmin", '_blank');
}

function exceldhkpadmin() {
	window.open(site_url + "dhkp/exceladmin", '_blank');
}

function pdfrealisasi() {
	window.open(site_url + "dhkp/mpdfrealisasi", '_blank');
}

function excelealisasi() {
	window.open(site_url + "dhkp/excelrealisasi", '_blank');
}

$('#pdfdesa').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkpdesa/mpdfdesa");
	$('#ipladder').submit();
});

$('#exceldesa').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkpdesa/exceldesa");
	$('#ipladder').submit();
});

$('#pdfdhkpdetail').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkp/mpdfdetail");
	$('#ipladder').submit();
});

$('#exceldhkpdetail').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkp/exceldetail");
	$('#ipladder').submit();
});

$('#pdfrealisasikec').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkpkec/mpdfrealisasikec");
	$('#ipladder').submit();
});

$('#excelrealisasikec').click(function () {
	$('#ipladder').attr('action', site_url + "dhkpkec/excelrealisasikec");
	$('#ipladder').submit();
});

$('#pdfrealisasidesa').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkpdesa/mpdfrealisasidesa");
	$('#ipladder').submit();
});

$('#excelrealisasidesa').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkpdesa/excelrealisasidesa");
	$('#ipladder').submit();
});

$('#pdfperalihan').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkp/mpdfperalihan");
	$('#ipladder').submit();
});

$('#excelperalihan').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkp/excelperalihan");
	$('#ipladder').submit();
});

$('#pdfrealisasidetail').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkp/mpdfrealisasidetail");
	$('#ipladder').submit();
});

$('#excelrealisasidetail').click(function () {
	$('#ipladder').attr('target', '_blank');
	$('#ipladder').attr('action', site_url + "dhkp/excelrealisasidetail");
	$('#ipladder').submit();
});

function rubah(angka) {
	var reverse = angka.toString().split('').reverse().join(''),
		ribuan = reverse.match(/\d{1,3}/g);
	ribuan = ribuan.join('.').split('').reverse().join('');
	return ribuan;
}

